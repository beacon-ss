/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_reference_countable_H
#define BEACONS_reference_countable_H

#include <boost/noncopyable.hpp>

extern "C" {
#include <atomic_ops.h>
}

namespace beacons {

/**
 * A convinience class that implements atomic reference counting.
 * When a class inherits from this class, it can be used with boost::intrusive_ptr
 * or beacons::intrusive_ptr to implement a thread-safe non-blocking
 * shared pointer.
 */
class reference_countable : private boost::noncopyable {
    friend void intrusive_ptr_add_ref(reference_countable *);
    friend void intrusive_ptr_release(reference_countable *);
    friend void beacons_intrusive_ptr_add_ref(reference_countable *);
    friend void beacons_intrusive_ptr_release(reference_countable *);

    protected:

        /**
         * Creates new instance, initializing the reference count to 0.
         */
        reference_countable() {
            AO_store(&_ref_cnt, 0);
        }

        virtual ~reference_countable() {}

    private:

        /**
         * Increases the reference count.
         */
        inline void _ref_incr() const {
            AO_fetch_and_add1(&_ref_cnt);
        }

        /**
         * Decreases the reference count.
         */
        inline void _ref_decr() const {
            if (AO_fetch_and_sub1(&_ref_cnt) <= 1) {
                delete this;
            }
        }

        /**
         * Current reference count.
         */
        mutable AO_t _ref_cnt;
};

/**
 * A function neccessary for the boost::intrusive_ptr to work.
 */
inline void intrusive_ptr_add_ref(reference_countable * p) {
  p->_ref_incr();
}

/**
 * A function neccessary for the boost::intrusive_ptr to work.
 */
inline void intrusive_ptr_release(reference_countable * p) {
  p->_ref_decr();
}

/**
 * A function neccessary for the beacons::intrusive_ptr to work.
 */
inline void beacons_intrusive_ptr_add_ref(reference_countable * p) {
  p->_ref_incr();
}

/**
 * A function neccessary for the beacons::intrusive_ptr to work.
 */
inline void beacons_intrusive_ptr_release(reference_countable * p) {
  p->_ref_decr();
}

} //namespace beacons

#endif
