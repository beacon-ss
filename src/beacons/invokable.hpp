/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_INVOKABLE_H
#define BEACONS_INVOKABLE_H

#include <beacons/reference_countable.hpp>
#include <beacons/detail/quick_wait.hpp>
#include <beacons/intrusive_ptr.hpp>

#include <boost/thread/recursive_mutex.hpp>

extern "C" {
#include <atomic_ops.h>
}

namespace beacons {

/**
 * This class represents the token that identifies an invokable in
 * \a event_queue::dequeue.
 */
class invocation_token : public reference_countable {

    public:

        /** The type of the lock for the \a guard. */
        typedef boost::recursive_mutex::scoped_lock lock_t;

        invocation_token(bool valid = true) : _valid(valid) {
        }

        ~invocation_token() {
        }

        /**
         * A true return value indicates that the invokable accompanied
         * by this token is valid.
         */
        bool valid() const {
            return _valid;
        }

        /**
         * This causes the \a valid method to return false, indicating that
         * the invokable accompanied by this token is no longer valid.
         * The guard is locked during the method invocation.
         */
        void invalidate() {
            lock_t l(_guard);
            _valid = false;
        }

        /**
         * The guard can be used to lock access to the token.
         */
        boost::recursive_mutex const & guard() const {
            return _guard;
        }

        /**
         * The guard can be used to lock access to the token.
         */
        boost::recursive_mutex & guard() {
            return _guard;
        }
    private:
        bool _valid;
        boost::recursive_mutex _guard;
};

/**
 * An abstract class that anything invokable from within an
 * {@link event_queue} needs to implement.
 */
class invokable {
    public:

        typedef intrusive_ptr<invocation_token> token_ptr;
        typedef invocation_token token_type;

    protected:

        /**
         * Creates new instance of an invokable object.
         * @param token a token used to match different invokables
         */
        invokable(token_ptr token) : _token(token) {
        }

    public:

        virtual ~invokable() {
        }

        virtual void invoke() = 0;

        token_ptr token() const {
            return _token;
        }

    private:
        token_ptr _token;
};

}

#endif
