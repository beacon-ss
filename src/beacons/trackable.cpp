/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#include <beacons/trackable.hpp>
#include <beacons/detail/quick_wait.hpp>
#include <algorithm>

namespace beacons {

    static inline void disconnect(intrusive_ptr<detail::connection_impl_base> & con) {
        con->disconnect();
    }

    trackable::~trackable() {
        detail::wait(_cons_guard);
        std::for_each(_connections.begin(), _connections.end(), &disconnect);
        detail::notify(_cons_guard);
    }

    trackable & trackable::operator=(trackable const & rhs) {

        detail::wait(_cons_guard);
        std::for_each(_connections.begin(), _connections.end(), &disconnect);
        _connections.clear();
        detail::notify(_cons_guard);

        _evq = rhs.current_event_queue();
        return *this;
    }

    void trackable::add_connection(intrusive_ptr<detail::connection_impl_base> con) {
        detail::wait(_cons_guard);
        _connections.push_back(con);
        detail::notify(_cons_guard);
    }
}
