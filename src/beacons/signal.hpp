/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_signal_H
#define BEACONS_signal_H

#include <beacons/fast_signal.hpp>
#include <beacons/combining_signal.hpp>

#endif
