/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_finished_future_H
#define BEACONS_finished_future_H

#include <beacons/future.hpp>

namespace beacons {

namespace detail {

template<typename R>
class finished_future : public future<R> {

    public:
        finished_future(R const & result) : _result(result) {
        }

        bool cancel() { return false; }

        bool finished() const { return true; }

        R get() throw (execution_exception) { return _result; }

        R get(int timeout) throw(execution_exception, timeout_exception) {
            return _result;
        }
    private:

        R _result;
};

template<>
class finished_future<void> : public future<void> {

    public:
        finished_future() {
        }

        bool cancel() { return false; }

        bool finished() const { return true; }

        void get() throw (execution_exception) {}

        void get(int timeout) throw(execution_exception, timeout_exception) {
        }

    private:
};

} //namespace detail

} //namespace beacons

#endif
