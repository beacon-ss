/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#include <beacons/detail/quick_wait.hpp>
#include <boost/thread.hpp>

namespace beacons {

namespace detail {

void wait(volatile AO_TS_t & guard) {
    while(AO_test_and_set(&guard) == AO_TS_SET) boost::thread::yield();
}

void notify(volatile AO_TS_t & guard) {
    AO_CLEAR(&guard);
}

} //namespace detail

}
