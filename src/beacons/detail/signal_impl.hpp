/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_signal_impl_H
#define BEACONS_signal_impl_H

#include <beacons/detail/signal_base.hpp>
#include <beacons/connection.hpp>
#include <beacons/detail/connection_impl.hpp>
#include <beacons/detail/slot.hpp>
#include <beacons/trackable.hpp>

namespace beacons {

namespace detail {

template<typename Signature>
class signal_impl : public signal_base {
    public:

        /**
         * Connects given function as a slot of this signal.
         * Note that a function connected using this method is
         * executed within the thread invoking the signal and
         * its lifetime is not tracked at all.
         * It therefore must be assured that the function is not
         * destroyed before all signals connected to it have been
         * destroyed (or at least no other invocations of the signals
         * will be made after the function is destroyed).
         */
        template<typename F>
        intrusive_ptr<connection> connect(F const & f) {
            return _connect(f, 0);
        }

        /**
         * Connects given function as a slot of this signal.
         * When this signal is invoked, the function invocation
         * is deferred to the event queue.
         * As for connect(F) the function's lifetime is not tracked
         * in any way so care must be taken when destroying
         * the function.
         */
        template<typename F>
        intrusive_ptr<connection> connect(F const &f, event_queue & queue) {
            return _connect(f, queue);
        }

        /**
         * Connects given function as a slot of this signal.
         * The supplied trackable is used to track the lifetime of the connection -
         * when the trackable is destroyed, the connection is disconnected.
         * It is left upon the obj's implementation to make sure that the
         * invocation of f is valid as long as it's alive.
         * If the obj's event queue ({@link trackable::current_event_queue}) is
         * not null, the invocations are defered to it, otherwise the function
         * is executed during the signal invocation in the same thread.
         */
        template<typename F>
        intrusive_ptr<connection> connect(F const & f, trackable & obj) {
            return _connect(f, &obj);
        }

    protected:
        signal_impl() {}

    private:
        template<typename F>
        intrusive_ptr<connection> _connect(F const & f, trackable * obj) {

            intrusive_ptr<slot_base> sl(new slot<F, Signature>(f));
            intrusive_ptr<connection_impl_base> ib = do_connect(sl, obj);

            connection_impl * ci = reinterpret_cast<connection_impl *>(ib.get());

            return intrusive_ptr<connection>(new connection(ci));
        }

        template<typename F>
        intrusive_ptr<connection> _connect(F const & f, event_queue & queue) {
            intrusive_ptr<slot_base> sl(new slot<F, Signature>(f));
            intrusive_ptr<connection_impl_base> ib = do_connect(sl, queue);

            connection_impl * ci = reinterpret_cast<connection_impl *>(ib.get());

            return intrusive_ptr<connection>(new connection(ci));
        }
};

} //namespace detail

} //namespace beacons

#endif
