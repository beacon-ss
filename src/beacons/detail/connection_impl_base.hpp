/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_connection_impl_base_H
#define BEACONS_connection_impl_base_H

#include <beacons/config.hpp>
#include <beacons/reference_countable.hpp>
#include <beacons/invokable.hpp>

namespace beacons {

namespace detail {

/**
 * Base class for connection implementation.
 * Defines the minimal interface that the connection
 * implementation supports.
 */
class connection_impl_base : public reference_countable {

    public:

        connection_impl_base() : _token(new invokable::token_ptr::item_type) {}

        virtual void disconnect() = 0;
        virtual bool connected() const = 0;

#ifdef BEACONS_ENABLE_CONNECTION_BLOCKING
        virtual bool blocked() const = 0;
#endif

        invokable::token_ptr token() const {
            return _token;
        }

    private:

        invokable::token_ptr _token;
};

} //namespace detail

} //namespace beacons

#endif
