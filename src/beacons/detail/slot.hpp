/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BOOST_PP_IS_ITERATING

#ifndef BEACONS_slot_H
#define BEACONS_slot_H

#include <beacons/config.hpp>
#include <beacons/reference_countable.hpp>

//include preprocessor magic
#include <boost/preprocessor/iteration/iterate.hpp>
#include <boost/preprocessor/repetition.hpp>
#include <boost/preprocessor/punctuation/comma_if.hpp>

#define BOOST_PP_ITERATION_LIMITS (0, BEACONS_MAX_ARGS)
#define BOOST_PP_FILENAME_1 "beacons/detail/slot.hpp" // this file

namespace beacons {

namespace detail {

class slot_base : public reference_countable {
    public:
        virtual ~slot_base() {}
};

template<typename Signature>
class slot_impl_base;

template<typename SlotFunction, typename Signature>
class slot;

#include BOOST_PP_ITERATE()

} //namespace detail

} //namespace beacons

#endif //BEACONS_slot_H

#else //BOOST_PP_IS_ITERATING

#define n BOOST_PP_ITERATION()

template<typename R
         BOOST_PP_COMMA_IF(n)
         BOOST_PP_ENUM_PARAMS(n, typename T)>
class slot_impl_base<R(*)(BOOST_PP_ENUM_PARAMS(n, T))> : public slot_base {
    public:

        virtual R operator()(BOOST_PP_ENUM_PARAMS(n, T)) const = 0;
};

template<typename SlotFunction,
         typename R
         BOOST_PP_COMMA_IF(n)
         BOOST_PP_ENUM_PARAMS(n, typename T)>
class slot<SlotFunction, R (*)(BOOST_PP_ENUM_PARAMS(n, T))> :
                 public slot_impl_base<R(*)(BOOST_PP_ENUM_PARAMS(n, T))> {

    public:

        slot(SlotFunction const & f) : _slot(f) {
            int some_dead_code = 0;
        }

        R operator()(BOOST_PP_ENUM_BINARY_PARAMS(n, T, arg)) const {
            return const_cast<SlotFunction &>(_slot)(BOOST_PP_ENUM_PARAMS(n, arg));
        }
    private:
        SlotFunction _slot;
};

template<typename SlotFunction
         BOOST_PP_COMMA_IF(n)
         BOOST_PP_ENUM_PARAMS(n, typename T)>
class slot<SlotFunction, void (*)(BOOST_PP_ENUM_PARAMS(n, T))> :
                 public slot_impl_base<void(*)(BOOST_PP_ENUM_PARAMS(n, T))> {

    public:

        slot(SlotFunction const & f) : _slot(f) {
            int some_dead_code = 0;
        }

        void operator()(BOOST_PP_ENUM_BINARY_PARAMS(n, T, arg)) const {
            const_cast<SlotFunction &>(_slot)(BOOST_PP_ENUM_PARAMS(n, arg));
        }
    private:
        SlotFunction _slot;
};
#endif //BOOST_PP_IS_ITERATING
