/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#include <beacons/detail/future_event_base.hpp>
#include <boost/thread/xtime.hpp>

namespace beacons {

namespace detail {

future_event_base::~future_event_base() {
    lock lk(monitor);
    value_set.notify_all();
}

bool future_event_base::cancel() {
    return false;
}

bool future_event_base::finished() const {
    return _finished;
}

void future_event_base::set(void * value) {
    lock lk(monitor);
    this->value = value;
    this->_finished = true;
    if (_waiting) {
        value_set.notify_all();
    }
}

void * future_event_base::get() throw(execution_exception) {
    lock lk(monitor);
    if (!_finished) {
        _waiting = true;

        value_set.wait(lk);

        _waiting = false;

        if (!_finished) {
            throw execution_exception("future destroyed before value was obtained");
        }
    }
    return value;
}

void * future_event_base::get(int timeout) throw(execution_exception, timeout_exception) {
    lock lk(monitor);
    if (_finished) {
        return value;
    } else {
        boost::xtime now;
        boost::xtime_get(&now, boost::TIME_UTC);

        now.sec += timeout / 1000;

        int nsec_incr = (timeout % 1000) * 1000;

        unsigned long incr = now.nsec + nsec_incr;

        if (incr >= 1000000) {
            now.sec += 1;
            nsec_incr = incr - 1000000;
        }
        now.nsec += nsec_incr;

        _waiting = true;
        if (value_set.timed_wait(lk, now)) {
            _waiting = false;
            if (!_finished) {
                throw execution_exception("future destroyed before value was obtained");
            }
            return value;
        } else {
            _waiting = false;
            throw timeout_exception();
        }
    }
}

} // namespace detail

}
