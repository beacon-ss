/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_signal_base_H
#define BEACONS_signal_base_H

#include <beacons/event_queue.hpp>
#include <beacons/intrusive_ptr.hpp>
#include <beacons/reference_countable.hpp>
#include <beacons/detail/slot.hpp>
#include <beacons/trackable.hpp>
#include <beacons/detail/connection_impl_base.hpp>
#include <beacons/detail/quick_wait.hpp>
#include <list>
#include <utility>

extern "C" {
#include <atomic_ops.h>
}

namespace beacons {

namespace detail {

//forward decl
class connection_impl;

class signal_base : public reference_countable {
    friend class connection_impl;
    public:

        //we represent the slot as just a slot_base * so that this
        //class doesn't have to be templated. The subclasses will
        //know what concrete slot type to cast it to...
        struct slot_list_item {
            intrusive_ptr<slot_base> slot;
            intrusive_ptr<connection_impl_base> connection;
            event_queue * queue;

            slot_list_item() : slot(0), connection(), queue(0) {}
        };
        typedef std::list<slot_list_item> slot_list_type;

        ~signal_base() {
            clear();
        }

        /**
         * Disconnects all slots from this signal.
         */
        void clear();

    protected:

        signal_base() : _slots(), _slots_guard(AO_TS_INITIALIZER) {}

        /**
         * Initializes the connection.
         */
        intrusive_ptr<connection_impl_base> do_connect(intrusive_ptr<slot_base> slot, trackable * obj);

        /**
         * Initializes the connection.
         */
        intrusive_ptr<connection_impl_base> do_connect(intrusive_ptr<slot_base> slot, event_queue & queue);

        /**
         * Removes the connection record identified by the iterator from the
         * slot list.
         */
        void do_disconnect(slot_list_type::iterator pos);

        /**
         * Returns a copy of the slot list as it was at the time
         * of the call. This operation is thread safe as it made sure
         * that no modifications are possible while making this copy.
         */
        slot_list_type get_slots_copy();

        /**
         * Direct access to the slot list.
         */
        inline slot_list_type & get_slots() {
            return _slots;
        }

    private:

        intrusive_ptr<connection_impl_base> _init_connection(slot_list_item & item);

        slot_list_type _slots;

        AO_TS_t _slots_guard;
};

} //namespace detail

} //namespace beacons

#endif
