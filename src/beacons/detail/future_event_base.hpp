/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_DETAIL_FUTURE_EVENT_BASE_H
#define BEACONS_DETAIL_FUTURE_EVENT_BASE_H

#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>
#include <beacons/exceptions.hpp>
#include <beacons/reference_countable.hpp>

namespace beacons {

namespace detail {

//forward decl
class in_queue_invoke_base;

/**
 * A base class for future_event class that implements the event_loop
 * aware functionality inside the library.
 */
class future_event_base : public reference_countable {
    friend class in_queue_invoke_base;

    typedef boost::mutex::scoped_lock lock;

    public:
        future_event_base() : value(0), _finished(false),
                          _waiting(false) {
        }

        ~future_event_base();
    protected:

        /** currently always fails returning false */
        bool cancel();
        bool finished() const;

        void set(void * value);
        void * get() throw (execution_exception);
        void * get(int timeout) throw(execution_exception, timeout_exception);

    private:
        boost::mutex monitor;
        boost::condition value_set;
        void * value;
        bool _finished;
        bool _waiting;
};

} //namespace detail

} //namespace

#endif
