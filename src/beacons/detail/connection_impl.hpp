/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_connection_impl_H
#define BEACONS_connection_impl_H

#include <beacons/config.hpp>
#include <beacons/intrusive_ptr.hpp>
#include <beacons/detail/connection_impl_base.hpp>
#include <beacons/detail/signal_base.hpp>

extern "C" {
#include <atomic_ops.h>
}

namespace beacons {

namespace detail {

class connection_impl : public connection_impl_base {
    friend class signal_base;
    public:
        bool connected() const {
            return _connected == AO_TS_CLEAR;
        }

#ifdef BEACONS_ENABLE_CONNECTION_BLOCKING
        bool blocked() const {
            return _blocked;
        }

        void block(bool do_block = true) {
            _blocked = do_block;
        }

        void unblock() {
            _blocked = false;
        }
#endif

        void disconnect() {
            //if the previous value was connected and the connection is still valid,
            //disconnect
            if (AO_test_and_set(&_connected) == AO_TS_CLEAR && token()->valid()) {
                _signal->do_disconnect(_position);
            }
        }

    private:

        connection_impl(signal_base * signal, signal_base::slot_list_type::iterator pos) :
            _signal(signal),
            _position(pos),
            _connected(AO_TS_CLEAR)
#ifdef BEACONS_ENABLE_CONNECTION_BLOCKING
            ,_blocked(false)
#endif
            {}

        signal_base * _signal;
        signal_base::slot_list_type::iterator _position;

        volatile AO_TS_t _connected; //AO_TS_CLEAR == connected, AO_TS_SET = disconnected
#ifdef BEACONS_ENABLE_CONNECTION_BLOCKING
        bool _blocked;
#endif
};

} //namespace detail

} //namespace beacons

#endif
