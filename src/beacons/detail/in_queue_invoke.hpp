/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BOOST_PP_IS_ITERATING

#ifndef BEACONS_in_queue_invoke_new_H
#define BEACONS_in_queue_invoke_new_H

#include <beacons/config.hpp>
#include <beacons/invokable.hpp>
#include <beacons/future_event.hpp>
#include <beacons/detail/future_event_base.hpp>
#include <beacons/reference_countable.hpp>

#include <boost/type_traits/function_traits.hpp>
#include <boost/type_traits/add_pointer.hpp>

//include the preprocessor magic
#include <boost/preprocessor/iteration/iterate.hpp>
#include <boost/preprocessor/repetition.hpp>
#include <boost/preprocessor/punctuation/comma_if.hpp>
#include <boost/preprocessor/cat.hpp>

#define BOOST_PP_ITERATION_LIMITS (0, BEACONS_MAX_ARGS)
#define BOOST_PP_FILENAME_1 "beacons/detail/in_queue_invoke.hpp" // this file

namespace beacons {

//forward decl
class event_queue;

namespace detail {

/**
 * Generic base class for the numerous specializations of the in_queue_invoke
 * class for various template parameters.
 */
class in_queue_invoke_base {
    protected:

        typedef intrusive_ptr<future_event_base> future_ptr;

        in_queue_invoke_base(future_ptr future) : _future(future) {}

        void set_result(void * result) {
            _future->set(result);
        }

    private:
        future_ptr _future;
};

//template classes to support different flavours of function
//objects invokable in the event_queue.

template<typename Function, typename Signature>
class in_queue_invoke;

template<typename Function, typename Signature>
class in_queue_invoke_no_future;

template<typename Function, typename Signature>
class in_queue_wrapper_future_pointer;

template<typename Function, typename Signature>
class in_queue_wrapper_no_future;

#include BOOST_PP_ITERATE()

} //namespace detail

} //namespace beacons

#endif //BEACONS_in_queue_invoke_new_H

#else // BOOST_PP_IS_ITERATING

#define n BOOST_PP_ITERATION()

#define CONSTRUCTOR_ARG_INIT(z, n, unused) \
    BOOST_PP_CAT(_arg, n)(BOOST_PP_CAT(arg, n))

#define MEMBER_VAR_DEF(z, n, unused) \
    BOOST_PP_CAT(T, n) BOOST_PP_CAT(_arg, n);

template<typename Function,
         typename R
         BOOST_PP_COMMA_IF(n)
         BOOST_PP_ENUM_PARAMS(n, typename T)>
class in_queue_invoke<Function, R (*)(BOOST_PP_ENUM_PARAMS(n, T))> :
    public beacons::invokable, public in_queue_invoke_base {

    public:

        explicit in_queue_invoke(
            BOOST_PP_ENUM_BINARY_PARAMS(n, T, arg)
            BOOST_PP_COMMA_IF(n)
            Function const * f,
            intrusive_ptr<future_event_base> future,
            invokable::token_ptr token)
            :
            invokable(token),
            in_queue_invoke_base(future),
            _f(f)
            BOOST_PP_COMMA_IF(n)
            BOOST_PP_ENUM(n, CONSTRUCTOR_ARG_INIT, ~) {}

        void invoke() {
            R result = (*_f)(BOOST_PP_ENUM_PARAMS(n, _arg));
            set_result(static_cast<void *>(&result));
        }

    private:
        Function const * _f;
        BOOST_PP_REPEAT(n, MEMBER_VAR_DEF, ~)
};

template<typename Function
         BOOST_PP_COMMA_IF(n)
         BOOST_PP_ENUM_PARAMS(n, typename T)>
class in_queue_invoke<Function, void (*)(BOOST_PP_ENUM_PARAMS(n, T))> :
    public beacons::invokable, public in_queue_invoke_base {

    public:

        explicit in_queue_invoke(
            BOOST_PP_ENUM_BINARY_PARAMS(n, T, arg)
            BOOST_PP_COMMA_IF(n)
            Function const * f,
            intrusive_ptr<future_event_base> future,
            invokable::token_ptr token)
            :
            invokable(token),
            in_queue_invoke_base(future),
            _f(f)
            BOOST_PP_COMMA_IF(n)
            BOOST_PP_ENUM(n, CONSTRUCTOR_ARG_INIT, arg) {}

        void invoke() {
            (*_f)(BOOST_PP_ENUM_PARAMS(n, _arg));
            void * res = 0;
            set_result(res);
        }

    private:
        Function const * _f;
        BOOST_PP_REPEAT(n, MEMBER_VAR_DEF, ~)
};

template<typename Function,
         typename R
         BOOST_PP_COMMA_IF(n)
         BOOST_PP_ENUM_PARAMS(n, typename T)>
class in_queue_invoke_no_future<Function, R (*)(BOOST_PP_ENUM_PARAMS(n, T))> :
    public beacons::invokable {

    public:
        explicit in_queue_invoke_no_future(
            BOOST_PP_ENUM_BINARY_PARAMS(n, T, arg)
            BOOST_PP_COMMA_IF(n)
            Function const * f,
            invokable::token_ptr token)
            :
            invokable(token),
            _f(f)
            BOOST_PP_COMMA_IF(n)
            BOOST_PP_ENUM(n, CONSTRUCTOR_ARG_INIT, arg) {}

        void invoke() {
            (*_f)(BOOST_PP_ENUM_PARAMS(n, _arg));
        }

    private:
        Function const * _f;
        BOOST_PP_REPEAT(n, MEMBER_VAR_DEF, ~)

};

template<typename Function,
         typename R
         BOOST_PP_COMMA_IF(n)
         BOOST_PP_ENUM_PARAMS(n, typename T)>
class in_queue_wrapper_future_pointer<Function, R (*)(BOOST_PP_ENUM_PARAMS(n, T))> {

    typedef R (* Signature)(BOOST_PP_ENUM_PARAMS(n, T));
    typedef intrusive_ptr<future<R> > result_type;
    public:

        explicit in_queue_wrapper_future_pointer(event_queue & queue, Function const * f, invokable::token_ptr token) :
            _queue(queue), _f(f), _token(token) {}

        result_type operator()(BOOST_PP_ENUM_BINARY_PARAMS(n, T, arg)) {
            intrusive_ptr<future_event_base> fut(new future_event<R>);

            this->_queue.enqueue(new in_queue_invoke<Function, Signature>(
                BOOST_PP_ENUM_PARAMS(n, arg)
                BOOST_PP_COMMA_IF(n)
                _f, fut, _token));

            return result_type(reinterpret_cast<future_event<R> *>(fut.get()));
        }

    private:
        event_queue & _queue;
        Function const * _f;
        invokable::token_ptr _token;
};

template<typename Function,
         typename R
         BOOST_PP_COMMA_IF(n)
         BOOST_PP_ENUM_PARAMS(n, typename T)>
class in_queue_wrapper_no_future<Function, R (*)(BOOST_PP_ENUM_PARAMS(n, T))> {

    typedef R (* Signature)(BOOST_PP_ENUM_PARAMS(n, T));
    public:

        explicit in_queue_wrapper_no_future(event_queue & queue, Function const * f, invokable::token_ptr token) :
            _queue(queue), _f(f), _token(token) {}

        void operator()(BOOST_PP_ENUM_BINARY_PARAMS(n, T, arg)) {
            this->_queue.enqueue(
                new in_queue_invoke_no_future<Function, Signature>(
                    BOOST_PP_ENUM_PARAMS(n, arg)
                    BOOST_PP_COMMA_IF(n)
                    _f, _token));
        }

    private:
        event_queue & _queue;
        Function const * _f;
        invokable::token_ptr _token;
};

#undef n
#undef CONSTRUCTOR_ARG_INIT
#undef MEMBER_VAR_DEF

#endif //BOOST_PP_IS_ITERATING
