/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_QUICK_WAIT_H
#define BEACONS_QUICK_WAIT_H

extern "C" {
#include <atomic_ops.h>
}

namespace beacons {

namespace detail {

//causes the current thread to wait until the guard is clear
void wait(volatile AO_TS_t & guard);

//clears the guard, ending the critical section
void notify(volatile AO_TS_t & guard);

} //namespace detail

} //namespace

#endif
