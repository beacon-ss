/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#include <beacons/detail/signal_base.hpp>
#include <beacons/detail/connection_impl.hpp>

namespace beacons {

namespace detail {

//a helper function to signal_base::do_disconnect and signal_base::clear
static inline void invalidate_slot(signal_base::slot_list_type::iterator & pos) {
    invokable::token_ptr token = pos->connection->token();
    token->invalidate();
    if (pos->queue != 0) {
        pos->queue->dequeue(token);
    }
}

//a helper for do_connect methods
intrusive_ptr<connection_impl_base> signal_base::_init_connection(signal_base::slot_list_item & item) {

    _slots.push_back(item);

    connection_impl * inited = new connection_impl(this, --_slots.end());

    intrusive_ptr<connection_impl_base> ip(inited);

    slot_list_item & ret = _slots.back();
    ret.connection.swap(ip);

    return intrusive_ptr<connection_impl_base>(ret.connection);
}

intrusive_ptr<connection_impl_base> signal_base::do_connect(intrusive_ptr<slot_base> slot, trackable * obj) {

    slot_list_item item;
    item.slot = slot;
    if (obj != 0) {
        item.queue = obj->current_event_queue();
    }

    wait(_slots_guard);

    intrusive_ptr<connection_impl_base> ret = _init_connection(item);

    //this must be inside lock so that we can be sure that
    //we do not disconnect the connection before it was inited
    //in the trackable, otherwise we could have races.
    if (obj != 0) {
        obj->add_connection(ret);
    }

    notify(_slots_guard);

    return ret;
}

intrusive_ptr<connection_impl_base> signal_base::do_connect(intrusive_ptr<slot_base> slot, event_queue & queue) {
    slot_list_item item;
    item.slot = slot;
    item.queue = &queue;

    wait(_slots_guard);

    intrusive_ptr<connection_impl_base> ret = _init_connection(item);

    notify(_slots_guard);

    return ret;
}

void signal_base::do_disconnect(slot_list_type::iterator pos) {
    wait(_slots_guard);
    invalidate_slot(pos);
    _slots.erase(pos);
    notify(_slots_guard);
}

void signal_base::clear() {
    wait(_slots_guard);

    for(slot_list_type::iterator it = _slots.begin(); it != _slots.end(); ++it) {
        invalidate_slot(it);
    }

    _slots.clear();

    notify(_slots_guard);
}

signal_base::slot_list_type signal_base::get_slots_copy() {
    wait(_slots_guard);
    slot_list_type ret = _slots;
    notify(_slots_guard);
    return ret;
}

} //namespace detail

} //namespace beacons
