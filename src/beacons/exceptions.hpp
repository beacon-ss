/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_EXCEPTIONS_H
#define BEACONS_EXCEPTIONS_H

#include <exception>
#include <string>
#include <iostream>
#include <typeinfo>
#include <vector>
#include <algorithm>
#include <boost/bind.hpp>

namespace beacons {

using namespace std;

struct exception_cause {
    exception_cause(char const * mes, char const * tn) : message(mes), type_name(tn) {}

    char const * message;
    char const * type_name;
    //some more diag?
};

/**
 * Base class for all exceptions thrown in beacon library.
 */
class exception : public std::exception {
public:

    explicit exception() throw() {}
    explicit exception(string const & message) throw() : _message(message) {}
    explicit exception(exception const & cause) throw() {
        init_causes(cause);
    }

    explicit exception(string const & message, exception const & cause) throw() : _message(message) {
        init_causes(cause);
    }

    ~exception() throw() {}

    string const message() const throw() {
        return _message;
    }

    char const * what() const throw() {
        return _message.c_str();
    }

    vector<exception_cause> const causes() const throw () {
        return vector<exception_cause>(_causes);
    }

    void print_stack_trace() {
        print_stack_trace(std::cerr);
    }

    void print_stack_trace(std::ostream & os) {
        os << typeid(*this).name() << " : " << what() << endl;
        os << "Caused by:" << endl;
        for(vector<exception_cause>::iterator i = _causes.begin(); i != _causes.end(); ++i) {
            exception_cause & cause = *i;
            os << cause.type_name << " : " << cause.message << endl;
        }
    }
private:

    void init_causes(exception const & ex) {
        type_info const & t = typeid(ex);
        _causes.push_back(exception_cause(ex.message().c_str(), t.name()));
        for_each(ex.causes().begin(), ex.causes().end(),
                 boost::bind(&vector<exception_cause>::push_back, _causes, _1));
    }

    string _message;
    vector<exception_cause> _causes;
};

/**
 * Thrown when some asynchronous operation times out.
 */
class timeout_exception : public exception
{
public:
    timeout_exception(std::string message, exception cause) : exception(message, cause) {}
    timeout_exception(exception const& cause) : exception(cause) {}
    timeout_exception(std::string message) : exception(message) {}
    timeout_exception() {}
};

/**
 * Thrown when some asynchronous operation fails.
 */
class execution_exception : public exception
{
public:
    execution_exception(std::string message, exception cause) : exception(message, cause) {}
    execution_exception(exception const& cause) : exception(cause) {}
    execution_exception(std::string message) : exception(message) {}
    execution_exception() {}
};

} //namespace

#endif
