/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_EVENT_LOOP_H
#define BEACONS_EVENT_LOOP_H

#include <beacons/event_queue.hpp>
#include <boost/thread.hpp>

namespace beacons {

/**
 * Event loop is a wrapper of event_queue that implements
 * a simple event loop. I.e. it starts a thread in which
 * all the events are invoked. There is no need to call \a event_queue::invoke_enqueued
 * or \a event_queue::invoke_synchronously once an event_loop
 * is started.
 */
class event_loop : public event_queue {
public:

    /**
     * A new thread is started to handle this event loop.
     */
    event_loop() :
               _thread(0),
               _running(false),
               _finished(false)
    {}

    /**
     * This call does NOT block until the underlying thread finishes.
     * It just destroys the event_loop object without stopping
     * the underlying thread. Be sure to call @link join method before
     * the event_loop is destroyed.
     */
    ~event_loop() {
        if (_thread) delete _thread;
    }

    /**
     * Starts the event loop. The events can be enqueued and dequeued before
     * the event loop is started but they won't be processed until it is.
     *
     * This method has no effect if there already is a thread running and
     * IS NOT thread-safe.
     */
    void start();

    /**
     * Joins the event loop thread. If the thread is started using the {@link start} method
     * while another thread is executing this method, the result is undefined.
     *
     * This method IS NOT thread-safe.
     */
    void join();

private:

    //the thread we're executing the events in
    boost::thread * _thread;

    //controlling the thread alive-state
    bool _running;
    bool _finished;

    //main loop
    void run();
};

} //namespage

#endif
