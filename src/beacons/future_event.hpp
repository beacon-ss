/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_FUTURE_EVENT_H
#define BEACONS_FUTURE_EVENT_H

#include <beacons/future.hpp>
#include <beacons/detail/future_event_base.hpp>

namespace beacons {

/**
 * An implementation of the future interface that cooperates with
 * {@link event_loop}.
 */
template<typename ResultType>
class future_event : public future<ResultType>, public detail::future_event_base {
    public:

        future_event() : future_event_base() {
        }

        /** currently always fails returning false */
        bool cancel() {
            return this->future_event_base::cancel();
        }

        bool finished() const {
            return this->future_event_base::finished();
        }

        ResultType get() throw (execution_exception) {
            ResultType * ret = static_cast<ResultType *>(this->future_event_base::get());

            return *ret;
        }

        ResultType get(int timeout) throw(execution_exception, timeout_exception) {
            ResultType * ret = static_cast<ResultType *>(this->future_event_base::get(timeout));

            return *ret;
        }
};

template<>
class future_event<void> : public future<void>, public detail::future_event_base {
    public:

        future_event() : future_event_base() {
        }

        /** currently always fails returning false */
        bool cancel() {
            return this->future_event_base::cancel();
        }

        bool finished() const {
            return this->future_event_base::finished();
        }

        void get() throw (execution_exception) {
            this->future_event_base::get();
        }

        void get(int timeout) throw(execution_exception, timeout_exception) {
            this->future_event_base::get();
        }
};

}

#endif
