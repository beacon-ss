/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BOOST_PP_IS_ITERATING

#ifndef BEACONS_combinable_signal_H
#define BEACONS_combinable_signal_H

#include <beacons/config.hpp> //for BEACONS_MAX_ARGS
#include <beacons/connection.hpp>
#include <beacons/event_queue.hpp>
#include <beacons/detail/slot.hpp>
#include <beacons/trackable.hpp>
#include <beacons/future.hpp>
#include <beacons/combiner/noop_combiner.hpp>
#include <beacons/detail/finished_future.hpp>
#include <beacons/detail/connection_impl_base.hpp>
#include <beacons/detail/signal_impl.hpp>
#include <boost/type_traits/function_traits.hpp>
#include <boost/type_traits/add_pointer.hpp>

#include <list>

//include preprocessor magic
#include <boost/preprocessor/iteration/iterate.hpp>
#include <boost/preprocessor/repetition.hpp>
#include <boost/preprocessor/punctuation/comma_if.hpp>
#include <boost/preprocessor/cat.hpp>

#define BOOST_PP_ITERATION_LIMITS (0, BEACONS_MAX_ARGS)
#define BOOST_PP_FILENAME_1 "beacons/combining_signal.hpp" // this file

namespace beacons {

namespace detail {

template<int Arity, typename Signature, typename Combiner>
struct get_combining_signal_impl;

#include BOOST_PP_ITERATE()

} //namespace detail

/**
 * Combining signal is a signal that can execute the slots
 * within an event queue of the trackables their connected
 * with.
 * The supplied combiner is fed the future result values of
 * the computations (see \a future).
 * The combiner must satisfy following contract:
 * 1) it must have a default constructor,
 * 2) it must define:
 * \code
 * typedef ... result_type;
 * \endcode
 * 3) it must define:
 * \code
 * template<typename InputIterator>
 * result_type operator()(InputIterator first, InputIterator last);
 * \endcode
 * where the InputIterator is a forward iterator over
 * \code
 * beacon::intrusive_ptr<beacon::future<R> >
 * \endcode
 * where R is the result type of the Signature.
 */
template<typename Signature,
    typename Combiner = noop_combiner>
class combining_signal :
        public detail::get_combining_signal_impl<
                (boost::function_traits<Signature>::arity),
                 Signature, Combiner>::type {

    public:

        /**
         * Creates a new signal with the same connections as this instance.
         * !!! Use with caution as there is no way of controlling the
         * individual connections of the new signal.
         */
        combining_signal<Signature, Combiner> & copy() {
            combining_signal<Signature, Combiner> * ret = new combining_signal<Signature, Combiner>();
            detail::signal_base::slot_list_type slts = detail::signal_base::get_slots_copy();

            for(detail::signal_base::slot_list_type::iterator it = slts.begin();
                    it != slts.end(); ++it) {

                ret->do_connect(it->slot, *(it->queue));
            }

            return *ret;
        }

};

} //namespace beacons

#endif //BEACON_fast_signal_H

#else //BOOST_PP_IS_ITERATING

#define n BOOST_PP_ITERATION()

#define COMBINING_SIGNAL_IMPL_N BOOST_PP_CAT(combining_signal_impl, n)

template<typename Signature,
         typename Combiner,
         typename CombinerResult
         BOOST_PP_COMMA_IF(n)
         BOOST_PP_ENUM_PARAMS(n, typename T)>
class COMBINING_SIGNAL_IMPL_N : public signal_impl<typename boost::add_pointer<Signature>::type> {

        typedef typename boost::function_traits<Signature>::result_type R;
        typedef slot_impl_base<typename boost::add_pointer<Signature>::type> slot_type;
        typedef intrusive_ptr<future<R> > future_result;

    public:
        typedef CombinerResult result_type;

        /**
         * Emits the signal.
         * All the connected slots are invoked with given parameters.
         * The invocation of the slot is relayed to the event queue if
         * the slot was connected using a trackable with non-null event queue
         * or executed directly if the trackable was null or its event queue
         * was null.
         * The future results are captured and fed to the combiner.
         */
        result_type raise(BOOST_PP_ENUM_BINARY_PARAMS(n, T, arg)) {
#ifdef BEACONS_SINGLE_THREADED
            signal_base::slot_list_type & slts = signal_base::get_slots();
#else
            signal_base::slot_list_type slts(signal_base::get_slots_copy());
#endif
            std::list<future_result> results;

            for(signal_base::slot_list_type::iterator it = slts.begin();
                    it != slts.end(); ++it) {

                intrusive_ptr<connection_impl_base> con = it->connection;

                if (con->connected()
#ifdef BEACONS_ENABLE_CONNECTION_BLOCKING
                    && !con->blocked()
#endif
                   ) {

                    slot_type * slot = reinterpret_cast<slot_type *>(it->slot.get());

#ifdef BEACONS_SINGLE_THREADED
                    future_result res(new finished_future<R>(
                        (*slot)(BOOST_PP_ENUM_PARAMS(n, arg))));

                    results.push_back(res);
#else
                    if (it->queue != 0) {
                        results.push_back(
                            enqueue<Signature>::invoke(*(it->queue), *slot, con->token())
                                (BOOST_PP_ENUM_PARAMS(n, arg)));
                    } else {
                        invokable::token_type::lock_t lock(con->token()->guard());

                        future_result res(new finished_future<R>(
                            (*slot)(BOOST_PP_ENUM_PARAMS(n, arg))));

                        results.push_back(res);
                    }
#endif
                }
            }

            Combiner combiner;

            return combiner(results.begin(), results.end());
        }

        /**
         * Calls raise() with the provided arguments.
         */
        result_type operator()(BOOST_PP_ENUM_BINARY_PARAMS(n, T, arg)) {
            return raise(BOOST_PP_ENUM_PARAMS(n, arg));
        }
};

//partial specialization for combiners returning void

template<typename Signature,
         typename Combiner
         BOOST_PP_COMMA_IF(n)
         BOOST_PP_ENUM_PARAMS(n, typename T)>
class COMBINING_SIGNAL_IMPL_N<
                Signature,
                Combiner,
                void
                BOOST_PP_COMMA_IF(n)
                BOOST_PP_ENUM_PARAMS(n, T)> : public signal_impl<typename boost::add_pointer<Signature>::type> {

        typedef typename boost::function_traits<Signature>::result_type R;
        typedef slot_impl_base<typename boost::add_pointer<Signature>::type> slot_type;
        typedef intrusive_ptr<future<R> > future_result;

    public:
        typedef void result_type;

        /**
         * Emits the signal.
         * All the connected slots are invoked with given parameters.
         * The invocation of the slot is relayed to the event queue if
         * the slot was connected using a trackable with non-null event queue
         * or executed directly if the trackable was null or its event queue
         * was null.
         * The future results are captured and fed to the combiner.
         */
        result_type raise(BOOST_PP_ENUM_BINARY_PARAMS(n, T, arg)) {
#ifdef BEACONS_SINGLE_THREADED
            signal_base::slot_list_type & slts = signal_base::get_slots();
#else
            signal_base::slot_list_type slts(signal_base::get_slots_copy());
#endif

            std::list<future_result> results;

            for(signal_base::slot_list_type::iterator it = slts.begin();
                    it != slts.end(); ++it) {

                intrusive_ptr<connection_impl_base> con = it->connection;

                if (con->connected()
#ifdef BEACONS_ENABLE_CONNECTION_BLOCKING
                    && !con->blocked()
#endif
                   ) {

                    slot_type * slot = reinterpret_cast<slot_type *>(it->slot.get());

#ifdef BEACONS_SINGLE_THREADED
                    //invoke the slot that returns void
                    (*slot)(BOOST_PP_ENUM_PARAMS(n, arg));

                    //create a "dummy" future for the void result
                    future_result res(new finished_future<R>());

                    results.push_back(res);
#else
                    if (it->queue != 0) {
                        results.push_back(
                            enqueue<Signature>::invoke(*(it->queue), *slot, con->token())
                                (BOOST_PP_ENUM_PARAMS(n, arg)));
                    } else {
                        invokable::token_type::lock_t lock(con->token()->guard());

                        //invoke the slot that returns void
                        (*slot)(BOOST_PP_ENUM_PARAMS(n, arg));

                        //create a "dummy" future for the void result
                        future_result res(new finished_future<R>());

                        results.push_back(res);
                    }
#endif
                }
            }

            Combiner combiner;

            combiner(results.begin(), results.end());
        }

        /**
         * Calls raise() with the provided arguments.
         */
        result_type operator()(BOOST_PP_ENUM_BINARY_PARAMS(n, T, arg)) {
            raise(BOOST_PP_ENUM_PARAMS(n, arg));
        }
};

#define ARG_TYPE(z, x, unused) \
    BOOST_PP_CAT(BOOST_PP_CAT( \
        typename boost::function_traits<Signature>::arg, \
        BOOST_PP_INC(x)), _type)

template<typename Signature, typename Combiner>
struct get_combining_signal_impl<n, Signature, Combiner> {

    typedef COMBINING_SIGNAL_IMPL_N<
            Signature,
            Combiner,
            typename Combiner::result_type
            BOOST_PP_COMMA_IF(n)
            BOOST_PP_ENUM(n, ARG_TYPE, ~)> type;
};

#undef n
#undef COMBINING_SIGNAL_IMPL_N
#undef ARG_TYPE

#endif //BOOST_PP_IS_ITERATING
