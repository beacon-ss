/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_intrusive_ptr_H
#define BEACONS_intrusive_ptr_H

#include <beacons/reference_countable.hpp>
#include <functional>           // for std::less
#include <iosfwd>               // for std::basic_ostream

extern "C" {
#include <atomic_ops.h>
}

namespace beacons {

/**
 * A simple shared pointer implementation inspired by
 * boost::intrusive_ptr that only adds a cas() operation to
 * the usual mix of ops.
 *
 * It depends on two functions being overriden for type T:
 * \c
 * beacon_intrusive_ptr_add_ref(T *)
 * beacon_intrusive_ptr_release(T *)
 * \endcode
 *
 * The first is called when a reference count should be increased,
 * the second when it should be decreased. It is the user's
 * responsibility to implement those functions properly.
 *
 * beacons::reference_countable implements those functions so
 * that any reference_countable instance is usable with this shared
 * pointer implementation.
 */
template<typename T>
class intrusive_ptr {

    public:

        typedef T item_type;

        intrusive_ptr() : _p(0) {}

        intrusive_ptr(T * p, bool add_ref = true) : _p(p) {
            if (_p != 0 && add_ref) {
                beacons_intrusive_ptr_add_ref(_p);
            }
        }

        intrusive_ptr(intrusive_ptr const & rhs) : _p(rhs._p) {
            if (_p != 0) beacons_intrusive_ptr_add_ref(_p);
        }

        ~intrusive_ptr() {
            if (_p != 0) beacons_intrusive_ptr_release(_p);
        }

        intrusive_ptr & operator=(intrusive_ptr const & rhs) {
            intrusive_ptr<T>(rhs).swap(*this);
            return *this;
        }

        intrusive_ptr & operator=(T * rhs) {
            intrusive_ptr<T>(rhs).swap(*this);
            return *this;
        }

        T * get() const {
            return _p;
        }

        T & operator*() const {
            return *_p;
        }

        T * operator->() const {
            return _p;
        }

        void swap(intrusive_ptr & rhs) {
            T * tmp = _p;
            _p = rhs._p;
            rhs._p = tmp;
        }

        typedef T * intrusive_ptr<T>::*unspecified_bool_type;

        operator unspecified_bool_type () const {
            return _p == 0? 0: &intrusive_ptr<T>::_p;
        }

        /**
         * Atomically does:
         * \code
         * if (this == cmp) {
         *    this = xchg;
         *    return true;
         * }
         * return false;
         * \endcode
         *
         * @p cmp the shared_ptr to compare with
         * @p xchg the shared_ptr to change this instance to
         */
        bool cas(intrusive_ptr<T> & cmp, intrusive_ptr<T> & xchg) {
            AO_t * my_ao = (AO_t *)(&_p);
            AO_t cmp_ao = (AO_t)(cmp.get());
            AO_t xchg_ao = (AO_t)(xchg.get());

            if (AO_compare_and_swap(my_ao, cmp_ao, xchg_ao)) {
                //I assigned the xchg pointer to me, tossing away
                //my former reference that used to be the same as cmp's.
                beacons_intrusive_ptr_add_ref(_p);
                beacons_intrusive_ptr_release((T *)cmp_ao);

                return true;
            }

            return false;
        }

    private:
        T * _p;
};

template<class T, class U> inline bool operator==(intrusive_ptr<T> const & a, intrusive_ptr<U> const & b)
{
    return a.get() == b.get();
}

template<class T, class U> inline bool operator!=(intrusive_ptr<T> const & a, intrusive_ptr<U> const & b)
{
    return a.get() != b.get();
}

template<class T, class U> inline bool operator==(intrusive_ptr<T> const & a, U * b)
{
    return a.get() == b;
}

template<class T, class U> inline bool operator!=(intrusive_ptr<T> const & a, U * b)
{
    return a.get() != b;
}

template<class T, class U> inline bool operator==(T * a, intrusive_ptr<U> const & b)
{
    return a == b.get();
}

template<class T, class U> inline bool operator!=(T * a, intrusive_ptr<U> const & b)
{
    return a != b.get();
}

template<class T> inline bool operator<(intrusive_ptr<T> const & a, intrusive_ptr<T> const & b)
{
    return std::less<T *>()(a.get(), b.get());
}

template<class T> void swap(intrusive_ptr<T> & lhs, intrusive_ptr<T> & rhs)
{
    lhs.swap(rhs);
}

// mem_fn support

template<class T> T * get_pointer(intrusive_ptr<T> const & p)
{
    return p.get();
}

template<class T, class U> intrusive_ptr<T> static_pointer_cast(intrusive_ptr<U> const & p)
{
    return static_cast<T *>(p.get());
}

template<class T, class U> intrusive_ptr<T> const_pointer_cast(intrusive_ptr<U> const & p)
{
    return const_cast<T *>(p.get());
}

template<class T, class U> intrusive_ptr<T> dynamic_pointer_cast(intrusive_ptr<U> const & p)
{
    return dynamic_cast<T *>(p.get());
}

template<class E, class T, class Y> std::basic_ostream<E, T> & operator<< (std::basic_ostream<E, T> & os, intrusive_ptr<Y> const & p)
{
    os << p.get();
    return os;
}

} //namespace beacons

#endif
