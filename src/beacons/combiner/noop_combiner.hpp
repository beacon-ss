/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_noop_combiner_H
#define BEACONS_noop_combiner_H

namespace beacons {

/**
 * The simplest combiner possible that returns void and
 * performs no operation on the supplied results.
 */
struct noop_combiner {
    typedef void result_type;

    template<typename InputIterator>
    result_type operator()(InputIterator first, InputIterator last) {}
};

} //namespace beacons

#endif
