/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_list_combiner_H
#define BEACONS_list_combiner_H

#include <beacons/future.hpp>
#include <beacons/intrusive_ptr.hpp>
#include <list>

namespace beacons {

template<typename R>
struct list_combiner {
    typedef std::list<intrusive_ptr<future<R> > > result_type;

    template<typename InputIterator>
    result_type operator()(InputIterator first, InputIterator last) {
        return result_type(first, last);
    }
};

} //namespace beacons

#endif
