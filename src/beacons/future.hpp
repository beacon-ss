/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_FUTURE_H
#define BEACONS_FUTURE_H

#include <beacons/exceptions.hpp>
#include <beacons/reference_countable.hpp>

namespace beacons {

/**
 * A future is an object referencing a result of a computation
 * that might have not been executed yet.
 */
template<typename ResultType>
class future : public reference_countable {

    public:
        typedef ResultType result_type;

        virtual ~future() {}

        /**
         * Tries to cancel the future computation. This method has no effect if the computation is already in progress.
         * @return true if cancellation was successful, false otherwise.
         */
        virtual bool cancel() = 0;

        /**
         * @return true if the computation has finished, false otherwise.
         */
        virtual bool finished() const = 0;

        /**
         * Gets the result of the computation.
         *
         * @throw execution_exception when the computation fails.
         */
        virtual ResultType get() throw(execution_exception) = 0;

        /**
         * Gets the result of the computation.
         *
         * @param timeout the maximum time in milliseconds to wait for the result.
         *
         * @throw execution_exception when the computation fails.
         * @throw timeout_exception if more then specified number of milliseconds elapsed
         * and the computation isn't finished yet.
         */
        virtual ResultType get(int timeout) throw(execution_exception, timeout_exception) = 0;
};

/**
 * scoped_future is a wrapper around another future pointer.
 * The pointer is destroyed once the scoped_future goes out of scope.
 */
template<typename R>
class scoped_future : public future<R> {
    public:

    scoped_future(future<R> * f) : _f(f) {
    }

    ~scoped_future() {
        delete _f;
    }

    bool cancel() {
        return _f->cancel();
    }

    bool finished() const {
        return _f->finished();
    }

    R get() throw (execution_exception) {
        return _f->get();
    }

    R get(int timeout) throw(execution_exception, timeout_exception) {
        return _f->get(timeout);
    }

    private:
        future<R> * _f;
};

template<>
class scoped_future<void> : public future<void> {
    public:

    scoped_future(future<void> * f) : _f(f) {
    }

    ~scoped_future() {
        delete _f;
    }

    bool cancel() {
        return _f->cancel();
    }

    bool finished() const {
        return _f->finished();
    }

    void get() throw (execution_exception) {
        _f->get();
    }

    void get(int timeout) throw(execution_exception, timeout_exception) {
        _f->get(timeout);
    }

    private:
        future<void> * _f;
};

}

#endif
