/**
 * signals
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_TRACKABLE_H
#define BEACONS_TRACKABLE_H

#include <beacons/event_queue.hpp>
#include <beacons/reference_countable.hpp>
#include <beacons/intrusive_ptr.hpp>
#include <beacons/detail/connection_impl_base.hpp>
#include <list>

extern "C" {
#include <atomic_ops.h>
}

namespace beacons {

//forward decl
namespace detail { class signal_base; }

/**
 * Trackable objects are automatically tracked by the library.
 * I.e. the connections are automatically disconnected
 * when a trackable object is destroyed. Each trackable object also belongs to
 * an event queue in which its slots are invoked.
 */
class trackable : public reference_countable {
    friend class detail::signal_base;
    public:
        trackable(event_queue * eq) : _evq(eq), _connections(),
                  _cons_guard(AO_TS_INITIALIZER) {}

        /**
         * The copy constructor of a trackable doesn't make the copy a part
         * of the connections of the original.
         */
        trackable(trackable const & other) :
                _evq(other.current_event_queue()),
                _connections(),
                _cons_guard(AO_TS_INITIALIZER) {}

        /**
         * Disconnects all the connections this trackable is part of.
         */
        ~trackable();

        /**
         * The connections are not transfered from trackable to trackable.
         * Upon assignment this trackable will have the same event loop
         * but empty will have no signals connected to it.
         */
        trackable & operator=(trackable const & rhs);

        /**
         * The event queue the slots of this trackable will
         * be executed in.
         * Can be null, which means that the slots are called
         * immediately during the signal invocation instead of
         * queuing in some event_queue.
         */
        event_queue * current_event_queue() const {
            return _evq;
        }

    protected:

        /**
         * Called during new connection creation.
         */
        void add_connection(intrusive_ptr<detail::connection_impl_base> con);

    private:
        event_queue * _evq;

        //list of connections this trackable is part (destination) of
        std::list<intrusive_ptr<detail::connection_impl_base> > _connections;

        //guarding the access to the connection list
        AO_TS_t _cons_guard;
};

}

#endif
