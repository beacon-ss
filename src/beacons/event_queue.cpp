/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#include <beacons/event_queue.hpp>
#include <beacons/detail/quick_wait.hpp>

#include <algorithm>

namespace beacons {


//used by dequeue method to decide whether to remove an invokable from the queue
static inline bool token_not_equal(invokable::token_ptr & token, event_queue::ev_ptr inv) {
    return token.get() != inv->token().get();
}

static void invoke_and_delete(event_queue::ev_ptr inv) {
    try {
        invokable::token_ptr const & token = inv->token();

        invokable::token_type::lock_t lock(token->guard());

        if (token->valid()) {
            inv->invoke();
        }

    } catch (...) {
        //TODO figure out what to do here
    }
    delete inv;
}

void delete_invokable(event_queue::ev_ptr inv) {
    delete inv;
}

event_queue::~event_queue() {
    detail::wait(_queue_guard);

    std::for_each(_events.begin(), _events.end(), &invoke_and_delete);
    _events.clear();

    std::for_each(_dequeued.begin(), _dequeued.end(), &delete_invokable);
    _dequeued.clear();

    //don't unlock the queue so that nothing can modify it anymore...
}

void event_queue::enqueue(ev_ptr inv) {
    //enter the critical section
    detail::wait(_queue_guard);

    _events.push_back(inv);

    //leave the critical section
    detail::notify(_queue_guard);
}

void event_queue::dequeue(invokable::token_ptr token) {
    //enter the critical section
    detail::wait(_queue_guard);

    std::deque<ev_ptr>::iterator end = _events.end();

    std::deque<ev_ptr>::iterator first_bad =
            stable_partition(_events.begin(), end,
                             boost::bind(&token_not_equal, token, _1));

    _dequeued.insert(first_bad, end);

    _events.erase(first_bad, end);

    //leave the critical section
    detail::notify(_queue_guard);
}

void event_queue::invoke_enqueued() {
    detail::wait(_queue_guard);

    std::deque<ev_ptr> evs_cpy;
    std::set<ev_ptr> deq_cpy;

    unsigned evs = _events.size();
    unsigned des = _dequeued.size();

    //check if there's anything to do at all...
    if ((evs | des) == 0) {
        detail::notify(_queue_guard);
        return;
    }

    //copy the current events... this is to block the enquing as little
    //as possible.
    if (evs > 0) {
        evs_cpy = _events;
        _events.clear();
    }

    if (des > 0) {
        deq_cpy = _dequeued;
        _dequeued.clear();
    }

    detail::notify(_queue_guard);

    if (evs > 0) {
        std::for_each(evs_cpy.begin(), evs_cpy.end(), &invoke_and_delete);
    }

    if (des > 0) {
        std::for_each(deq_cpy.begin(), deq_cpy.end(), &delete_invokable);
    }
}

void event_queue::invoke_synchronously() {
    detail::wait(_queue_guard);

    std::for_each(_events.begin(), _events.end(), &invoke_and_delete);
    _events.clear();

    std::for_each(_dequeued.begin(), _dequeued.end(), &delete_invokable);
    _dequeued.clear();

    detail::notify(_queue_guard);
}

} //namespace beacons
