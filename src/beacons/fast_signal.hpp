/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BOOST_PP_IS_ITERATING

#ifndef BEACONS_fast_signal_H
#define BEACONS_fast_signal_H

#include <beacons/config.hpp> //for BEACON_MAX_ARGS
#include <beacons/connection.hpp>
#include <beacons/event_queue.hpp>
#include <beacons/detail/slot.hpp>
#include <beacons/trackable.hpp>
#include <beacons/detail/connection_impl_base.hpp>
#include <beacons/detail/signal_impl.hpp>
#include <boost/type_traits/function_traits.hpp>
#include <boost/type_traits/add_pointer.hpp>

//include preprocessor magic
#include <boost/preprocessor/iteration/iterate.hpp>
#include <boost/preprocessor/repetition.hpp>
#include <boost/preprocessor/punctuation/comma_if.hpp>
#include <boost/preprocessor/cat.hpp>

#define BOOST_PP_ITERATION_LIMITS (0, BEACONS_MAX_ARGS)
#define BOOST_PP_FILENAME_1 "beacons/fast_signal.hpp" // this file

namespace beacons {

namespace detail {

template<int Arity, typename Signature>
struct get_fast_signal_impl;

#include BOOST_PP_ITERATE()

} //namespace detail

/**
 * Fast signal is a signal that can execute the slots
 * within an event queue of the trackables their connected
 * with.
 * There is no support to retrieve or track the results
 * of those invocations (which makes the signal execution "fast").
 */
template<typename Signature>
class fast_signal :
        public detail::get_fast_signal_impl<
                (boost::function_traits<Signature>::arity),
                 Signature>::type {

    public:

        /**
         * Creates a new signal with the same connections as this instance.
         * !!! Use with caution as there is no way of controlling the
         * individual connections of the new signal.
         */
        fast_signal<Signature> & copy() {
            fast_signal<Signature> * ret = new fast_signal<Signature>;
            detail::signal_base::slot_list_type slts = detail::signal_base::get_slots_copy();

            for(detail::signal_base::slot_list_type::iterator it = slts.begin();
                    it != slts.end(); ++it) {

                ret->do_connect(it->slot, *(it->queue));
            }

            return *ret;
        }
};

} //namespace beacons

#endif //BEACONS_fast_signal_H

#else //BOOST_PP_IS_ITERATING

#define n BOOST_PP_ITERATION()

#define FAST_SIGNAL_IMPL_N BOOST_PP_CAT(fast_signal_impl, n)

template<typename Signature
         BOOST_PP_COMMA_IF(n)
         BOOST_PP_ENUM_PARAMS(n, typename T)>
class FAST_SIGNAL_IMPL_N : public detail::signal_impl<typename boost::add_pointer<Signature>::type> {
    public:
        typedef slot_impl_base<typename boost::add_pointer<Signature>::type> slot_type;

        /**
         * Emits the signal.
         * All the connected slots are invoked with given parameters.
         * The invocation of the slot is relayed to the event queue if
         * the slot was connected using a trackable with non-null event queue
         * or executed directly if the trackable was null or its event queue
         * was null.
         */
        void raise(BOOST_PP_ENUM_BINARY_PARAMS(n, T, arg)) {
#ifdef BEACONS_SINGLE_THREADED
            signal_base::slot_list_type & slts = signal_base::get_slots();
#else
            signal_base::slot_list_type slts(signal_base::get_slots_copy());
#endif
            for(signal_base::slot_list_type::iterator it = slts.begin();
                    it != slts.end(); ++it) {

                intrusive_ptr<connection_impl_base> con = it->connection;

                if (con->connected()
#ifdef BEACONS_ENABLE_CONNECTION_BLOCKING
                    && !con->blocked()
#endif
                   ) {

                    slot_type * slot = reinterpret_cast<slot_type *>(it->slot.get());

#ifdef BEACONS_SINGLE_THREADED
                    (*slot)(BOOST_PP_ENUM_PARAMS(n, arg));
#else
                    if (it->queue != 0) {
                        enqueue<Signature>::call(*(it->queue), *slot, con->token())
                            (BOOST_PP_ENUM_PARAMS(n, arg));
                    } else {
                        invokable::token_type::lock_t lock(con->token()->guard());

                        (*slot)(BOOST_PP_ENUM_PARAMS(n, arg));
                    }
#endif
                }
            }
        }

        /**
         * Calls raise() with the provided arguments.
         */
        inline void operator()(BOOST_PP_ENUM_BINARY_PARAMS(n, T, arg)) {
            raise(BOOST_PP_ENUM_PARAMS(n, arg));
        }
};

#define ARG_TYPE(z, x, unused) \
    BOOST_PP_CAT(BOOST_PP_CAT( \
        typename boost::function_traits<Signature>::arg, \
        BOOST_PP_INC(x)), _type)

template<typename Signature>
struct get_fast_signal_impl<n, Signature> {

    typedef FAST_SIGNAL_IMPL_N<
            Signature
            BOOST_PP_COMMA_IF(n)
            BOOST_PP_ENUM(n, ARG_TYPE, ~)> type;
};

#undef n
#undef FAST_SIGNAL_IMPL_N
#undef ARG_TYPE

#endif //BOOST_PP_IS_ITERATING
