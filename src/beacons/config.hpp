/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_config_H
#define BEACONS_config_H

//The configuration variables for the library

//the maximum number of function arguments supported by the library
#ifndef BEACONS_MAX_ARGS
#   define BEACONS_MAX_ARGS 10
#endif

//whether to enable connection blocking
#ifdef BEACONS_ENABLE_CONNECTION_BLOCKING
    //disabled by default
#endif

//whether the signals will only be used from a single thread.
#ifdef BEACONS_SINGLE_THREADED
    //disabled by default
#endif

#endif
