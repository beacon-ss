/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_event_queue_H
#define BEACONS_event_queue_H

#include <beacons/invokable.hpp>
#include <beacons/detail/in_queue_invoke.hpp>

#include <boost/type_traits/add_pointer.hpp>

#include <deque>
#include <set>

extern "C" {
#include <atomic_ops.h>
}

namespace beacons {

/**
 * Events can be enqueued or dequeued from the event queue and
 * the event queue is able to invoke currently enqueued events.
 *
 * All the public methods of this class are thread safe. The event_queue
 * is intended to be used concurrently.
 */
class event_queue {

    public:

        typedef invokable * ev_ptr;

        event_queue() : _queue_guard(AO_TS_INITIALIZER) {}

        /**
         * Synchronously invokes all the events in the queue to clean up.
         * The queue is never unlocked to ensure that concurrent modifications
         * are not possible before the event_queue is eventually destructed.
         */
        ~event_queue();

        /**
         * Enqueues given invokable in the queue.
         * <p>
         * The event queue takes over the ownership of the invokable once it is passed
         * to this method. Therefore the invokable must not be destroyed by the user code
         * once passed to this method. An invokable can only be enqueued once. Enqueuing it
         * more times will result in memory corruption and segfault.
         *
         * @param invokable the invokable object to put into the event loop queue.
         */
        void enqueue(ev_ptr invokable);

        /**
         * Removes invokables identified by given token from the event queue.
         *
         * @param token the token of the invokable to remove from the queue.
         */
        void dequeue(invokable::token_ptr token);

        /**
         * Invokes all the enqueued events removing them from the queue afterwards.
         * Care is taken to lock the queue only for a minimum time necessary.
         */
        void invoke_enqueued();

        /**
         * Invokes all the enqueued events synchronously. I.e. this method
         * blocks until all the events are invoked. During the execution
         * of this method no other threads can modify the event queue.
         *
         * This method can be useful in "teardown" scenarios where we need
         * to ensure that no modifications can be done to the queue.
         */
        void invoke_synchronously();

    private:

        //list of events to process
        std::deque<ev_ptr> _events;

        //list of recently dequeued events that need destroyed
        std::set<ev_ptr> _dequeued;

        volatile AO_TS_t _queue_guard;
};

/**
 * A helper class to enqueue function calls in the event queue.
 */
template<typename Signature>
struct enqueue {

    /**
     * A helper structure to represent the return type of the enqueue::invoke method
     * more easily.
     */
    template<typename F>
    struct invokable {
        typedef typename detail::in_queue_wrapper_future_pointer<F, typename boost::add_pointer<Signature>::type> type;
    };

    /**
     * A helper structure to represent the return type of the enqueue::call method
     * more easily.
     */
    template<typename F>
    struct callable {
        typedef typename detail::in_queue_wrapper_no_future<F, typename boost::add_pointer<Signature>::type> type;
    };

    /**
     * Using this function one can obtain a function object that
     * when invoked enqueues the actual computation in the provided
     * event queue and returns a shared pointer to a future object that can be used
     * to obtain the return value. The receiver is responsible for destroying
     * that future object afterwards.
     * @param eq the event_queue to be used
     * @param f the function to be called in event queue
     * @param token a token uniquely identifying the function. This token is
     * then used in event queue dequeue method.
     * @return a function object with operator () defined as:
     * \c beacons::intrusive_ptr<beacons::future<R> > operator()(ARGS)
     * \endcode, where R is the
     * return type of the supplied function and ARGS are its argument types.
     */
    template<typename F>
    static typename invokable<F>::type invoke(event_queue & eq, F const & f, beacons::invokable::token_ptr token) {
        return typename invokable<F>::type(eq, &f, token);
    }

    /**
     * Using this function one can obtain a function object that
     * when invoked enqueues the actual computation in the provided
     * event queue and is not able to query the result in any manner.
     * Note that this is by far the fastest method and should be used
     * preferably.
     * @param eq the event_queue to be used
     * @param f the function to be called in event queue
     * @param token a token uniquely identifying the function. This token is
     * then used in event_queue dequeue method.
     * @return a function object with operator () defined as:
     * \c void operator()(ARGS)
     * \endcode, where ARGS are argument types of the supplied function f.
     */
    template<typename F>
    static typename callable<F>::type call(event_queue & eq, F const & f, beacons::invokable::token_ptr token) {
        return typename callable<F>::type(eq, &f, token);
    }
};

} //namespace beacons

#endif
