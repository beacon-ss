/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACONS_CONNECTION_H
#define BEACONS_CONNECTION_H

#include <beacons/config.hpp>
#include <beacons/reference_countable.hpp>
#include <beacons/intrusive_ptr.hpp>
#include <beacons/detail/connection_impl.hpp>

namespace beacons {

//forward decl
namespace detail {
    template<typename Signature>
    class signal_impl;
}

/**
 * A wrapper around the actual connection implementation
 * that is returned to the user.
 * Non-createable - can be created only by the signal.
 * Copyable - all copies share the same underlying connection implementation.
 */
class connection : public reference_countable {

    template<typename Signature>
    friend class detail::signal_impl;

    public:

        bool connected() const {
            return _con->connected();
        }

        void disconnect() {
            _con->disconnect();
        }

#ifdef BEACONS_ENABLE_CONNECTION_BLOCKING
        void block() {
            _con->block();
        }

        void unblock() {
            _con->unblock();
        }

        bool blocked() const {
            return _con->blocked();
        }
#endif
    private:

        connection(intrusive_ptr<detail::connection_impl> con) : _con(con) {}

        intrusive_ptr<detail::connection_impl> _con;
};

/**
 * This connection type disconnects when it goes out of
 * scope.
 */
class scoped_connection : public connection {
    public:

        ~scoped_connection() {
            disconnect();
        }
};

} // namespace beacons

#endif
