/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#include <beacons/event_loop.hpp>
#include <boost/bind.hpp>

namespace beacons {

void event_loop::start() {
    if (_thread == 0) {
        _running = true;
        _finished = false;
        _thread = new boost::thread(boost::bind(&event_loop::run, this));
    }
}

void event_loop::join() {
    if (_thread != 0) {
        _running = false;

        while(!_finished) boost::thread::yield();

        _thread->join();

        delete _thread;

        _thread = 0;
    }
}

void event_loop::run() {
    while(_running) {

        invoke_enqueued();

        boost::thread::yield();
    }

    //flush the rest of the events before we die...

    invoke_synchronously();

    _finished = true;
}

} //namespace
