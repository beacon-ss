#include <QCoreApplication>
#include <QThread>
#include <QTimer>
#include "multi_thread_support.hpp"

#include <beacons/event_loop.hpp>
#include <beacons/signal.hpp>
#include <beacons/trackable.hpp>

#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>
#include <list>

class receiver : public beacons::trackable {
    public:

        receiver(size_t cnt, boost::condition * condition, beacons::event_queue * q) :
            beacons::trackable(q), _cnt(cnt), _num(cnt), _cond(condition) {}

        void receive() {
            if (_cnt == _num) {
                _start_time = boost::posix_time::microsec_clock::local_time();
            }
            if (--_cnt == 0) {
                boost::posix_time::ptime stop_time = boost::posix_time::microsec_clock::local_time();
                std::cout << "beacons: " << _num << " invocations took: " << std::fixed <<
                    _as_double(stop_time - _start_time)
                    << std::endl;

                _cond->notify_all();
            }
        }

    private:

        double _as_double(boost::posix_time::time_duration const & duration) {
            double result = duration.ticks();
            return result /= duration.ticks_per_second();
        }

        size_t _cnt;
        size_t _num;

        boost::posix_time::ptime _start_time;

        boost::condition * _cond;

};

class sender {

    public:

        beacons::fast_signal<void()> & send() {
            return _send_signal;
        }

    private:

        beacons::fast_signal<void()> _send_signal;
};

void sender_runner(sender * sender, size_t calls) {
    for(size_t i = 0; i < calls; ++i) {
        sender->send()();
    }
}

void launch(size_t iterations, size_t threads) {
    boost::mutex m;
    boost::mutex::scoped_lock lock(m);
    boost::condition waiter;

    beacons::event_loop receiver_thread;
    receiver r(iterations * threads, &waiter, &receiver_thread);
    receiver_thread.start();

    std::list<sender *> senders;

    for(size_t i = 0; i < threads; ++i) {
        sender * s = new sender;
        s->send().connect(boost::bind(&receiver::receive, &r), r);
        senders.push_back(s);
        boost::thread(boost::bind(&sender_runner, s, iterations));
    }
    //hopefully the receiver wasn't faster than me...
    waiter.wait(lock);

    receiver_thread.join();

    for(std::list<sender *>::iterator it = senders.begin(); it != senders.end(); ++it) {
        delete *it;
    }
}

int main(int argc, char ** argv) {
    size_t iterations = 10 * 9 * 8 * 7 * 6 * 5 * 4 * 3 * 2;
    size_t threads = atoi(argv[1]);

    iterations /= threads;

    launch(iterations, threads);

    QCoreApplication app(argc, argv);

    Launcher l(iterations, threads);

    l.launch();

/*    QTimer::singleShot(0, &l, SLOT(launch()));

    return app.exec();*/
}

