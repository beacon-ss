#include "boost/signal.hpp"
#include "boost/bind.hpp"
#include "boost/date_time/posix_time/posix_time_types.hpp"

#include <sigc++/sigc++.h>
#include <vector>
#include <cstdio>
#include <iostream>

#include "fastsig/fastsig.hpp"

#define BEACONS_SINGLE_THREADED
#include <beacons/fast_signal.hpp>

struct X
{
  X(int x = 0) : x_(x) { }
  int x_;
};


void foo(X & x)
{
  x.x_ += 1234;
}

void blarg(X & x)
{
  x.x_ /= 71;
}

struct bar
{
  void operator()(X & x) { x.x_ *= 7; }
};

struct foobar
{
  void doit(X & x) { x.x_ += 7; }
};

struct timing
{
  size_t nslots_;
  size_t ncalls_;
  double elapsed2_;
  double elapsed3_;
  double elapsed4_;
  double elapsed5_;
};

inline
double
as_double(
    boost::posix_time::time_duration const & duration)
{
  double result = duration.ticks();
  return result /= duration.ticks_per_second();
}

int
main(
    int argc,
    char * argv[])
{
  try
  {
    std::vector<timing> timings;
    size_t num_slots[] =
    {
      1, 10, 50, 100, 250, 500, 1000, 5000, 10000, 50000, 100000,
      500000, 0
    };

    typedef boost::signal<void (X &)> Signal2;
    Signal2 signal2;
    typedef sigc::signal<void, X&> Signal3;
    Signal3 signal3;
    typedef fastsig::signal<void(X &)> Signal4;
    Signal4 signal4;
    typedef beacons::fast_signal<void(X &)> Signal5;
    Signal5 signal5;

    size_t totcalls[] = { 1000, 10000, 100000, 1000000, 0 };
    for (size_t * tc = totcalls; *tc > 0; ++tc)
    {
      size_t total_calls = *tc;
      for (size_t * ns = num_slots; *ns > 0 && *ns <= total_calls; ++ns)
      {
        size_t nslots = *ns;
        size_t niters = total_calls / nslots;

        signal2.disconnect_all_slots();
        signal2.connect(bar());
        foobar foobar2;
        signal2.connect(boost::bind(&foobar::doit, &foobar2, _1));

        signal3.clear();
        signal3.connect( bar());
        foobar foobar3;
        signal3.connect( sigc::mem_fun( foobar3, &foobar::doit));

	signal4.clear();
	signal4.connect(bar());
	foobar foobar4;
	signal4.connect(boost::bind(&foobar::doit, &foobar4, _1));

	signal5.clear();
	signal5.connect(bar());
	foobar foobar5;
	signal5.connect(boost::bind(&foobar::doit, &foobar5, _1));

        std::vector<foobar> slots(nslots);
        for (size_t i = 0; i < slots.size(); ++i)
        {
          signal2.connect(boost::bind(&foobar::doit, &slots[i], _1));
          signal3.connect(sigc::mem_fun( slots[i], &foobar::doit));
	  signal4.connect(boost::bind(&foobar::doit, &slots[i], _1));
	  signal5.connect(boost::bind(&foobar::doit, &slots[i], _1));
        }

        timing t;
        t.nslots_ = nslots;
        t.ncalls_ = niters;
        X bs_x(5);
        boost::posix_time::ptime start_time = boost::posix_time::microsec_clock::local_time();
        for (size_t i = 0; i < niters; ++i)
        {
          signal2(bs_x);
        }
        boost::posix_time::ptime stop_time = boost::posix_time::microsec_clock::local_time();
        t.elapsed2_ = as_double(stop_time - start_time);

        X sc_x(5);
        start_time = boost::posix_time::microsec_clock::local_time();
        for (size_t i = 0; i < niters; ++i)
        {
          signal3(sc_x);
        }
        stop_time = boost::posix_time::microsec_clock::local_time();
        t.elapsed3_ = as_double(stop_time - start_time);

        X fs_x(5);
        start_time = boost::posix_time::microsec_clock::local_time();
        for (size_t i = 0; i < niters; ++i)
        {
          signal4(fs_x);
        }
        stop_time = boost::posix_time::microsec_clock::local_time();
        t.elapsed4_ = as_double(stop_time - start_time);

        X be_x(5);
        start_time = boost::posix_time::microsec_clock::local_time();
        for (size_t i = 0; i < niters; ++i)
        {
          signal5(be_x);
        }
        stop_time = boost::posix_time::microsec_clock::local_time();
        t.elapsed5_ = as_double(stop_time - start_time);

        timings.push_back(t);

        if (bs_x.x_ != sc_x.x_ || bs_x.x_ != fs_x.x_ || bs_x.x_ != be_x.x_)
        {
          std::cerr << "bs_x(" << bs_x.x_ << "),  sc_x("
              << sc_x.x_ << "), fs_x(" << fs_x.x_ << "), be_x(" << be_x.x_ << "\n";
        }
      }
    }
    size_t last_size = (size_t)-1;
    for (size_t i = 0; i < timings.size(); ++i)
    {
      if (last_size != timings[i].nslots_ * timings[i].ncalls_)
      {
        last_size = timings[i].nslots_ * timings[i].ncalls_;
        fprintf(stdout, "\n===== %u Total Calls =====\n", last_size);
        fprintf(stdout, "Num Slots    Calls/Slot    Boost     SigC      FastSig   beacons\n");
        fprintf(stdout, "---------    ----------    -------   -------   -------   -------\n");
      }
      fprintf(stdout, "%9u%14u%11.4f%10.4f%10.4f%10.4f\n",
          timings[i].nslots_,
          timings[i].ncalls_,
          timings[i].elapsed2_,
          timings[i].elapsed3_,
	  timings[i].elapsed4_,
	  timings[i].elapsed5_);
    }
    return 0;
  }
  catch (std::exception const & ex)
  {
    std::cerr << "exception: " << ex.what() << std::endl;
  }
  return 1;
}


