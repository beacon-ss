/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACON_multi_thread_support_H
#define BEACON_multi_thread_support_H

#include <QObject>
#include <QThread>
#include <QTimer>
#include <QCoreApplication>

#include <iostream> //debug

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>

class Receiver : public QObject {
    Q_OBJECT

    public:

        Receiver(size_t cnt, boost::condition * cond, QObject * parent = 0) : QObject(parent), _cnt(cnt), _num(cnt), _cond(cond) {}

    public slots:

        void receive() {
            if (_cnt == _num) {
                _start_time = boost::posix_time::microsec_clock::local_time();
            }
            if (--_cnt == 0) {
                boost::posix_time::ptime stop_time = boost::posix_time::microsec_clock::local_time();
                std::cout << "Qt: " << _num << " invocations took: " << std::fixed <<
                    _as_double(stop_time - _start_time)
                    << std::endl;
                _cond->notify_all();
            }
        }

    private:

        double _as_double(boost::posix_time::time_duration const & duration) {
            double result = duration.ticks();
            return result /= duration.ticks_per_second();
        }

        size_t _cnt;
        size_t _num;

        boost::posix_time::ptime _start_time;
        boost::condition * _cond;
};

class Sender : public QObject {
    Q_OBJECT

    public:

        Sender(QObject * parent = 0) : QObject(parent) {}

        void do_send() {
            emit send();
        }

    signals:

        void send();
};

class SenderRunner : public QThread {

    Q_OBJECT

    public:

        SenderRunner(QObject * parent, Sender * s, size_t calls) : QThread(parent), _sender(s), _calls(calls) {}

    protected:

        void run() {
            for(size_t i = 0; i < _calls; ++i) {
                _sender->do_send();
            }
        }

    private:

        Sender * _sender;
        size_t _calls;
};

class Launcher : public QObject {

    Q_OBJECT

    public:
        Launcher(size_t loops, size_t senders) : _loops(loops), _senders(senders) {}

    public slots:

        void launch() {
            boost::mutex m;
            boost::mutex::scoped_lock lock(m);
            boost::condition waiter;

            QThread * receiverThread = new QThread(this);

            Receiver * r = new Receiver(_loops * _senders, &waiter);

            r->moveToThread(receiverThread);

            receiverThread->start();

            for(size_t i = 0; i < _senders; ++i) {
                Sender * s = new Sender;
                QObject::connect(s, SIGNAL(send()), r, SLOT(receive()));
                SenderRunner * sr = new SenderRunner(this, s, _loops);
                sr->start();
            }

            waiter.wait(lock);

            receiverThread->exit();
            receiverThread->wait();
        }

    private:

        size_t _loops;
        size_t _senders;
};

#endif
