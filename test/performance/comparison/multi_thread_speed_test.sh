#!/bin/sh

echo "Thread    Qt        beacons"
echo "--------- --------- --------"

for threads in 1 2 3 4 5 6 7 8 9 10
do

	beacons_average=0
	qt_average=0
	attempt=0
	attempts=5
	while [ $attempt -lt $attempts ]
	do

		num=0
		for time in `./multi-thread-speed-test $threads | cut -d " " -f 5`
		do
			if [ $num -eq 0 ]
			then
				beacons_average=$(echo "scale=6;$beacons_average + $time / $attempts" | bc -l)

				num=1
			else
				qt_average=$(echo "scale=6;$qt_average + $time / $attempts" | bc -l)
			fi			
		done
		
		attempt=$(($attempt + 1))
	done

	echo "$threads $qt_average $beacons_average"
done
