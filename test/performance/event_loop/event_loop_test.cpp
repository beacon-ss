
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <beacons/event_loop.hpp>
#include <boost/bind.hpp>
#include <beacons/reference_countable.hpp>

using namespace std;
using namespace beacons;

unsigned times_called = 0;
class inv : public invokable {
    public:

        inv() : invokable(0) {
        }

        void invoke() {
            times_called++;
        }

        bool operator==(invokable const & other) {
            return this == &other;
        }
};

void function() {
    times_called++;
}

int main(int argc, char * argv[]) {

    event_loop el;

    el.start();

    inv in;

    beacons::invokable::token_ptr token(new invocation_token);

    //fast
    //enqueue<void()>::callable<void(*)()>::type fn = enqueue<void()>::call(el, &function, token);

    //slow - approx. 4x slower
    enqueue<void()>::invokable<void(*)()>::type fn = enqueue<void()>::invoke(el, &function, token);

    boost::xtime start, end, now;
    boost::xtime_get(&start, boost::TIME_UTC);

    for(int i = 0; i < 1000000; i++) {
        fn();
    }


//     boost::xtime_get(&now, boost::TIME_UTC);
//     now.nsec += 1000;
//     boost::thread::sleep(now);
//
//     cout << "dequing ";
//     cout << el.dequeue(token) << " calls" << endl;

    el.join();

    boost::xtime_get(&end, boost::TIME_UTC);

    double diff = end.sec - start.sec + (end.nsec - start.nsec) / 1e9;

    cout << "time: " << diff << std::endl;
    cout << "times called: " << times_called << endl;
    return EXIT_SUCCESS;
}
