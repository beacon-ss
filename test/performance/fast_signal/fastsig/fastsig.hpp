//
//  (C) Copyright 2006 Alexander Tsvyashchenko - http://www.ndl.kiev.ua
//  
//  Use, modification and distribution are subject to the
//  Boost Software License, Version 1.0. (See accompanying file
//  LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
#ifndef FASTSIG_HPP
#define FASTSIG_HPP

#include <fastsig/fastsig_common.hpp>
#include <fastsig/fastsig_iter.hpp>

#endif // FASTSIG_HPP
