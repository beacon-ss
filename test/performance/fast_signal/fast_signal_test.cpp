/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#include <beacons/fast_signal.hpp>
#include <beacons/event_loop.hpp>
#include <beacons/trackable.hpp>
#include <boost/bind.hpp>
#include <iostream>
#include <boost/signal.hpp>
#include <boost/thread/xtime.hpp>
#include <boost/thread.hpp>
#include <fastsig/fastsig.hpp>

#include <performance_test_output.hpp>

#include <cstdlib>
#include <vector>
#include <string>

#include <getopt.h>

size_t nof_event_loops = 0;
size_t nof_signals = 0;
size_t min_threads = 1;
size_t max_threads = 1;
size_t nof_loops = 0;
size_t test_runs = 0;

std::vector<beacons::event_loop *> event_loops;
std::vector<beacons::fast_signal<void ()> *> signals;

//payload function
void fn() {
    boost::thread cur_thread;
    boost::xtime now;
    boost::xtime_get(&now, boost::TIME_UTC);

    now.nsec += 1e4;
    if (now.nsec < 0) {
        now.sec += 1;
        now.nsec -= 1e4;
    }

    cur_thread.sleep(now);
}

//
//intializers
//

void connect_all_signals_no_event_loop() {
    for(size_t i = 0; i < nof_signals; ++i) {
        signals[i]->connect(&fn);
    }
}

void disconnect_all_signals() {
    for(size_t i = 0; i < nof_signals; ++i) {
        signals[i]->clear();
    }
}

void connect_all_signals_first_event_loop() {
    for(size_t i = 0; i < nof_signals; ++i) {
        signals[i]->connect(&fn, *event_loops[0]);
    }
}

void connect_all_signals_event_loops() {
    for(size_t i = 0; i < nof_signals; ++i) {
        int el_idx = rand() % nof_event_loops;
        signals[i]->connect(&fn, *event_loops[el_idx]);
    }
}

void start_first_event_loop_no_signals() {
    event_loops[0]->start();
}

void stop_first_event_loop_no_signals() {
    event_loops[0]->join();
}

void start_all_event_loops_no_signals() {
    for(size_t i = 0; i < nof_event_loops; ++i) {
        event_loops[i]->start();
    }
}

void stop_all_event_loops_no_signals() {
    for(size_t i = 0; i < nof_event_loops; ++i) {
        event_loops[i]->join();
    }
}

void start_first_event_loop() {
    connect_all_signals_first_event_loop();
    event_loops[0]->start();
}

void stop_first_event_loop() {
    event_loops[0]->join();
    disconnect_all_signals();
}

void start_all_event_loops() {
    connect_all_signals_event_loops();
    for(size_t i = 0; i < nof_event_loops; ++i) {
        event_loops[i]->start();
    }
}

void stop_all_event_loops() {
    for(size_t i = 0; i < nof_event_loops; ++i) {
        event_loops[i]->join();
    }
    disconnect_all_signals();
}

//
//tests
//

void boost_signal() {
    boost::signal<void ()> sig;

    sig.connect(&fn);

    for(size_t i = 0; i < nof_loops; ++i) {
        sig();
    }
}

void fastsig_signal() {
    fastsig::signal<void ()> sig;

    sig.connect(&fn);

    for(size_t i = 0; i < nof_loops; ++i) {
        sig();
    }
}

void fast_signal_no_event_loop() {
    beacons::fast_signal<void ()> sig;

    sig.connect(&fn);

    for(size_t i = 0; i < nof_loops; ++i) {
        sig();
    }
}

void all_fast_signals() {
    for(size_t i = 0; i < nof_loops; ++i) {
        int sig_idx = rand() % nof_signals;
        (*signals[sig_idx])();
    }
}

void one_fast_signal() {
    beacons::fast_signal<void ()> & sig = *signals[0];
    for(size_t i = 0; i < nof_loops; ++i) {
        sig();
    }
}

void short_lived_fast_signal_one_event_loop() {
    beacons::fast_signal<void ()> sig;

    sig.connect(&fn, *event_loops[0]);

    for(size_t i = 0; i < nof_loops; ++i) {
        sig();
    }
}

void short_lived_fast_signal_event_loops() {
    beacons::fast_signal<void ()> sig;

    int el_i = rand() % nof_event_loops;

    sig.connect(&fn, *event_loops[el_i]);

    for(size_t i = 0; i < nof_loops; ++i) {
        sig();
    }
}

void usage() {
    std::cout << "-h --help               this text" << std::endl
              << "-i --signal-invocations how many times to invoke a signal in each test" << std::endl
              << "-e --event-loops        how many event loops to start and use" << std::endl
              << "-s --signals            how many signals to define and use" << std::endl
              << "-r --test-runs          the number of test runs" << std::endl
              << "-l --min-threads        the minimum number of threads to run the tests in (>=1)" << std::endl
              << "-m --max-threads        the maximum number of threads to run the tests in" << std::endl
              << "-o --output             pmin, pavg, pmax, omin, oavg, omax" << std::endl;
}

int main(int argc, char * argv[]) {

    const char * short_opts = "hi:e:s:r:l:m:o:";
    const struct option long_opts[] = {
        {"help", 0, NULL, 'h'},
        {"signal-invocations", 1, NULL, 'i'},
        {"event-loops", 1, NULL, 'e'},
        {"signals", 1, NULL, 's'},
        {"test-runs", 1, NULL, 'r'},
        {"min-threads", 1, NULL, 'l'},
        {"max-threads", 1, NULL, 'm'},
        {"output", 1, NULL, 'o'}
    };

    int opt;

    bool pmin = false, pavg = false, pmax = false, omin = false, oavg = true, omax = false;

    if (argc == 1) {
        usage();
        return 0;
    }

    do {
        opt = getopt_long(argc, argv, short_opts, long_opts, NULL);
        switch (opt) {
            case 'h':
                usage();
                return 0;
            case 'i':
                nof_loops = atoi(optarg);
                break;
            case 'e':
                nof_event_loops = atoi(optarg);
                break;
            case 's':
                nof_signals = atoi(optarg);
                break;
            case 'r':
                test_runs = atoi(optarg);
                break;
            case 'l':
                min_threads = atoi(optarg);
                break;
            case 'm':
                max_threads = atoi(optarg);
                break;
            case 'o':
                std::string arg(optarg);
                pmin = arg.find("pmin") != std::string::npos;
                pavg = arg.find("pavg") != std::string::npos;
                pmax = arg.find("pmax") != std::string::npos;
                omin = arg.find("omin") != std::string::npos;
                oavg = arg.find("oavg") != std::string::npos;
                omax = arg.find("omax") != std::string::npos;
                break;
        }
    } while (opt != -1);

    for(size_t i = 0; i < nof_event_loops; ++i) {
        event_loops.push_back(new beacons::event_loop);
    }

    for(size_t i = 0; i < nof_signals; ++i) {
        signals.push_back(new beacons::fast_signal<void ()>);
    }

    beacons::test::performance_test pt;
    pt.runs(test_runs);
    pt.min_threads(min_threads);
    pt.max_threads(max_threads);

    pt.add_test("boost::signal", 0, boost_signal, 0);
    pt.add_test("fastsig::signal", 0, fastsig_signal, 0);
    pt.add_test("fast_signal no event loop", 0, fast_signal_no_event_loop, 0);
    pt.add_test("fast_signal one event loop", start_first_event_loop, one_fast_signal, stop_first_event_loop);
    pt.add_test("fast_signal all event loops", start_all_event_loops, all_fast_signals, stop_all_event_loops);
    pt.add_test("short lived fast_sginal one event loop", start_first_event_loop_no_signals, short_lived_fast_signal_one_event_loop, stop_first_event_loop_no_signals);
    pt.add_test("short lived fast_signal all event loops", start_all_event_loops_no_signals, short_lived_fast_signal_event_loops, stop_all_event_loops_no_signals);

    beacons::test::performance_test_csv_output out(pt.run());
    out.output_payload_min(pmin);
    out.output_payload_avg(pavg);
    out.output_payload_max(pmax);
    out.output_overall_min(omin);
    out.output_overall_avg(oavg);
    out.output_overall_max(omax);

    std::cout << out;

    for(size_t i = 0; i < nof_event_loops; ++i) {
        delete event_loops[i];
    }

    for(size_t i = 0; i < nof_signals; ++i) {
        delete signals[i];
    }

    return 0;
}
