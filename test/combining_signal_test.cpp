/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#include <beacons/combining_signal.hpp>

#define BOOST_TEST_MODULE combining_signal
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

using namespace beacons;

int times_called = 0;

void void_func() {
    times_called++;
}

int times_called_ret() {
    return ++times_called;
}

//testing summing combiner
template<typename Number>
struct sum_combiner {
    typedef Number result_type;

    template<typename InputIterator>
    Number operator()(InputIterator begin, InputIterator end) {
        Number result = 0;
        for(InputIterator i = begin; i != end; ++i) {
            intrusive_ptr<future<Number> > ptr = *i;
            future<Number> * fut = ptr.get();

            result += fut->get();
        }

        return result;
    }
};

BOOST_AUTO_TEST_CASE(simple) {
    times_called = 0;
    combining_signal<void()> sig;

    sig.connect(&void_func);

    sig();

    BOOST_CHECK(times_called == 1);
}

BOOST_AUTO_TEST_CASE(value_return) {
    times_called = 0;

    combining_signal<int(), sum_combiner<int> > sig;

    sig.connect(&times_called_ret);
    sig.connect(&times_called_ret);
    sig.connect(&times_called_ret);
    sig.connect(&times_called_ret);

    int result = sig();

    BOOST_CHECK(times_called == 4);
    BOOST_CHECK(result == 10);
}

BOOST_AUTO_TEST_CASE(copy_test) {
    times_called = 0;

    combining_signal<void()> sig;

    sig.connect(&void_func);

    combining_signal<void()> & copy = sig.copy();

    sig();
    copy();

    BOOST_CHECK(times_called == 2);

    sig.clear();

    sig();
    copy();

    BOOST_CHECK(times_called == 3);
}
