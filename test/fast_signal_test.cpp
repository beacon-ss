/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#include <beacons/fast_signal.hpp>
#include <boost/bind.hpp>
#define BOOST_TEST_MODULE fast_signal
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

using namespace beacons;

size_t func0_called = 0;
size_t func1_called = 0;
size_t func2_called = 0;

void func0() {
    func0_called++;
}

void func1(int) {
    func1_called++;
}

void func2(int, int) {
    func2_called++;
}

class disconnecting_slot {
    public:
    intrusive_ptr<connection> con;

    void run() {
        con->disconnect();
        func0();
    }
};

class trackable : public beacons::trackable {
    public:
        trackable(event_queue * q) : beacons::trackable(q) {}
};

BOOST_AUTO_TEST_CASE(call_no_event_queue) {

    func0_called = 0;

    fast_signal<void()> sig;

    sig.connect(&func0);

    sig();

    BOOST_CHECK(func0_called == 1);
}

BOOST_AUTO_TEST_CASE(disconnect_no_event_queue) {
    func0_called = 0;

    fast_signal<void()> sig;

    intrusive_ptr<connection> con = sig.connect(&func0);

    sig();

    con->disconnect();

    sig();

    BOOST_CHECK(func0_called == 1);
}

BOOST_AUTO_TEST_CASE(disconnect_during_call_no_event_queue) {
    func0_called = 0;

    fast_signal<void()> sig;

    disconnecting_slot sl;

    sl.con = sig.connect(boost::bind(&disconnecting_slot::run, &sl));

    sig();

    sig();

    BOOST_CHECK(func0_called == 1);
}

BOOST_AUTO_TEST_CASE(trackable_disconnect_no_event_loop) {

    func0_called = 0;

    fast_signal<void()> sig;

    ::trackable * track = new ::trackable(0);

    sig.connect(&func0, *track);

    sig();

    delete track;

    sig();

    BOOST_CHECK(func0_called == 1);
}

BOOST_AUTO_TEST_CASE(copy_test) {
    func0_called = 0;

    fast_signal<void()> sig;

    sig.connect(&func0);

    fast_signal<void()> & copy = sig.copy();

    sig();
    copy();

    BOOST_CHECK(func0_called == 2);

    sig.clear();

    sig();
    copy();

    BOOST_CHECK(func0_called == 3);
}
