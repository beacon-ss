/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACON_performance_test_H
#define BEACON_performance_test_H

#include <boost/thread.hpp>
#include <list>
#include <string>
#include <utility>

namespace beacons {

namespace test {

struct test_run_result {
    double payload_min;
    double payload_max;
    double payload_avg;
    double overall_min;
    double overall_max;
    double overall_avg;
};

struct test_result {
    std::string name;
    std::list<pair<int, test_run_result> > thread_results;
};

struct test_setup {
    std::string name;
    void (* setup_fn)();
    void (* payload_fn)();
    void (* teardown_fn)();

    test_setup() : setup_fn(0), payload_fn(0), teardown_fn(0) {}
};

class performance_test {

    public:

        size_t runs() const {
            return _runs;
        }

        void runs(size_t const & val) {
            _runs = val;
        }

        size_t min_threads() const {
            return _min_threads;
        }

        void min_threads(size_t const & val) {
            _min_threads = val;
        }

        size_t max_threads() const {
            return _max_threads;
        }

        void max_threads(size_t const & val) {
            _max_threads = val;
        }

        void add_test(std::string name, void(* setup)(), void(* payload)(), void( * teardown)()) {
            test_setup set;
            set.name = name;
            set.setup_fn = setup;
            set.payload_fn = payload;
            set.teardown_fn = teardown;
            _tests.push_back(set);
        }

        std::list<test_result> run() {
            std::list<test_result> results;

            for(std::list<test_setup>::iterator it = _tests.begin(); it != _tests.end(); ++it) {
                test_setup & test = *it;

                test_result result;
                result.name = test.name;

                for(size_t nof_threads = _min_threads; nof_threads <= _max_threads; ++nof_threads) {
                    result.thread_results.push_back(
                        make_pair(nof_threads, run_test(test, _runs, nof_threads)));
                }

                results.push_back(result);
            }

            return results;
        }
    private:

        test_run_result run_test(test_setup & setup, size_t const & runs, size_t const & nof_threads) {
            double test_sum_time = 0;
            double test_min_time = 0;
            double test_max_time = 0;
            double overall_sum_time = 0;
            double overall_min_time = 0;
            double overall_max_time = 0;

            for(size_t run = 0; run < runs; ++run) {
                boost::xtime overall_start, test_start, overall_end, test_end;

                boost::xtime_get(&overall_start, boost::TIME_UTC);

                if (setup.setup_fn != 0) {
                    setup.setup_fn();
                }

                boost::thread_group tg;

                boost::xtime_get(&test_start, boost::TIME_UTC);

                for(size_t i = 1; i < nof_threads; ++i) {
                    tg.create_thread(setup.payload_fn);
                }

                setup.payload_fn();

                tg.join_all();

                boost::xtime_get(&test_end, boost::TIME_UTC);

                if (setup.teardown_fn != 0) {
                    setup.teardown_fn();
                }

                boost::xtime_get(&overall_end, boost::TIME_UTC);

                double test_time = test_end.sec - test_start.sec + (test_end.nsec - test_start.nsec) / 1e9;
                double overall_time = overall_end.sec - overall_start.sec + (overall_end.nsec - overall_start.nsec) / 1e9;

                test_sum_time += test_time;
                overall_sum_time += overall_time;

                test_min_time = test_min_time == 0 ? test_time : (test_min_time < test_time ? test_min_time : test_time);
                test_max_time = test_max_time < test_time ? test_time : test_max_time;

                overall_min_time = overall_min_time == 0 ? overall_time : (overall_min_time < overall_time ? overall_min_time : overall_time);
                overall_max_time = overall_max_time < overall_time ? overall_time : overall_max_time;
            }

            test_run_result result;
            result.payload_min = test_min_time;
            result.payload_max = test_max_time;
            result.payload_avg = test_sum_time / runs;
            result.overall_min = overall_min_time;
            result.overall_max = overall_max_time;
            result.overall_avg = overall_sum_time / runs;

            return result;
        }

        std::list<test_setup> _tests;
        size_t _runs;
        size_t _min_threads;
        size_t _max_threads;
};

} //namespace test

} //namespace beacons

#endif
