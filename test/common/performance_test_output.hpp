/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#ifndef BEACON_performance_test_output_H
#define BEACON_performance_test_output_H

#include <performance_test.hpp>

#include <iosfwd>
#include <utility>

namespace beacons {

namespace test {

class performance_test_csv_output {
    template<typename E, typename T>
    friend std::basic_ostream<E, T> & operator <<(std::basic_ostream<E, T> &, performance_test_csv_output const &);

    public:
        performance_test_csv_output(std::list<test_result> const & results) :
            _output_payload_min(true),
            _output_payload_max(true),
            _output_payload_avg(true),
            _output_overall_min(true),
            _output_overall_max(true),
            _output_overall_avg(true),
            _results(results) {}

        void output_payload_min(bool theValue) {
            _output_payload_min = theValue;
        }

        bool output_payload_min() const {
            return _output_payload_min;
        }

        void output_payload_max(bool theValue) {
            _output_payload_max = theValue;
        }

        bool output_payload_max() const {
            return _output_payload_max;
        }

        void output_payload_avg(bool theValue) {
            _output_payload_avg = theValue;
        }

        bool output_payload_avg() const {
            return _output_payload_avg;
        }

        void output_overall_min(bool theValue) {
            _output_overall_min = theValue;
        }

        bool output_overall_min() const {
            return _output_overall_min;
        }

        void output_overall_max(bool theValue) {
            _output_overall_max = theValue;
        }

        bool output_overall_max() const {
            return _output_overall_max;
        }

        void output_overall_avg(bool theValue) {
            _output_overall_avg = theValue;
        }

        bool output_overall_avg() const {
            return _output_overall_avg;
        }

    private:
        bool _output_payload_min;
        bool _output_payload_max;
        bool _output_payload_avg;
        bool _output_overall_min;
        bool _output_overall_max;
        bool _output_overall_avg;
        std::list<test_result> _results;
};

template<typename E, typename T>
std::basic_ostream<E, T> & operator <<(std::basic_ostream<E, T> & str, performance_test_csv_output const & out) {
    size_t nof_columns = 0;
    if (out.output_payload_min()) nof_columns++;
    if (out.output_payload_max()) nof_columns++;
    if (out.output_payload_avg()) nof_columns++;
    if (out.output_overall_min()) nof_columns++;
    if (out.output_overall_max()) nof_columns++;
    if (out.output_overall_avg()) nof_columns++;

    if (nof_columns == 0) return str; //nothing to show

    //the list of iterators over each test results
    std::list<std::list<std::pair<int, test_run_result> >::const_iterator> result_iterators;

    std::list<std::pair<int, test_run_result> >::const_iterator last_result = out._results.front().thread_results.end();

    //output the header
    str << "\"Threads\",";

    std::list<test_result>::const_iterator last_item = --out._results.end();
    for(std::list<test_result>::const_iterator it = out._results.begin(); it != last_item;
        ++it) {
        str << "\"" << it->name << "\"";
        for(size_t i = 0; i < nof_columns; ++i) {
            str << ",";
        }

        std::list<pair<int, test_run_result> >::const_iterator result_it = it->thread_results.begin();

        result_iterators.push_back(result_it);
    }

    str << "\"" << last_item->name << "\"";
    for(size_t i = 0; i < nof_columns; ++i) {
        str << ",";
    }
    str << std::endl;

    //output individual column headers
    str << ",";
    for(std::list<test_result>::const_iterator it = out._results.begin(); it != last_item;
        ++it) {

        if (out.output_payload_min()) {
            str << "payload min,";
        }
        if (out.output_payload_avg()) {
            str << "payload avg,";
        }
        if (out.output_payload_max()) {
            str << "payload max,";
        }
        if (out.output_overall_min()) {
            str << "overall min,";
        }
        if (out.output_overall_avg()) {
            str << "overall avg,";
        }
        if (out.output_overall_max()) {
            str << "overall max,";
        }
    }

    size_t left_to_do = nof_columns;
    if (out.output_payload_min()) {
        str << "payload min";
        if (--left_to_do > 0) str << ",";
    }
    if (out.output_payload_avg()) {
        str << "payload avg";
        if (--left_to_do > 0) str << ",";
    }
    if (out.output_payload_max()) {
        str << "payload max";
        if (--left_to_do > 0) str << ",";
    }
    if (out.output_overall_min()) {
        str << "overall min";
        if (--left_to_do > 0) str << ",";
    }
    if (out.output_overall_avg()) {
        str << "overall avg";
        if (--left_to_do > 0) str << ",";
    }
    if (out.output_overall_max()) {
        str << "overall max";
        if (--left_to_do > 0) str << ",";
    }
    str << std::endl;

    std::list<pair<int, test_run_result> >::const_iterator result_it = last_item->thread_results.begin();
    result_iterators.push_back(result_it);

    //output the data
    std::list<std::list<std::pair<int, test_run_result> >::const_iterator>::const_iterator last_result_item = --result_iterators.end();
    bool more_data = true;
    while(more_data) {
        //output the current number of threads
        str << result_iterators.front()->first << ",";

        for(std::list<std::list<std::pair<int, test_run_result> >::const_iterator>::const_iterator it = result_iterators.begin();
            it != last_result_item; ++it) {

            test_run_result trr = (*it)->second;

            if (out.output_payload_min()) {
                str << trr.payload_min << ",";
            }
            if (out.output_payload_avg()) {
                str << trr.payload_avg << ",";
            }
            if (out.output_payload_max()) {
                str << trr.payload_max << ",";
            }
            if (out.output_overall_min()) {
                str << trr.overall_min << ",";
            }
            if (out.output_overall_avg()) {
                str << trr.overall_avg << ",";
            }
            if (out.output_overall_max()) {
                str << trr.overall_max << ",";
            }

            //advance the current result iterator;
            std::list<std::pair<int, test_run_result> >::const_iterator & res_it =
                    const_cast<std::list<std::pair<int, test_run_result> >::const_iterator &>(*it);
            ++res_it;
        }

        test_run_result trr = (*last_result_item)->second;
        size_t left_to_do = nof_columns;
        if (out.output_payload_min()) {
            str << trr.payload_min;
            if (--left_to_do > 0) str << ",";
        }
        if (out.output_payload_avg()) {
            str << trr.payload_avg;
            if (--left_to_do > 0) str << ",";
        }
        if (out.output_payload_max()) {
            str << trr.payload_max;
            if (--left_to_do > 0) str << ",";
        }
        if (out.output_overall_min()) {
            str << trr.overall_min;
            if (--left_to_do > 0) str << ",";
        }
        if (out.output_overall_avg()) {
            str << trr.overall_avg;
            if (--left_to_do > 0) str << ",";
        }
        if (out.output_overall_max()) {
            str << trr.overall_max;
            if (--left_to_do > 0) str << ",";
        }
        str << std::endl;

        std::list<std::pair<int, test_run_result> >::const_iterator & res_it =
                const_cast<std::list<std::pair<int, test_run_result> >::const_iterator &>(*last_result_item);
        ++res_it;

        //check if there's more data
        more_data = result_iterators.front() != last_result;
    }

    return str;
}

} //namespace test

} //namespace beacons

#endif
