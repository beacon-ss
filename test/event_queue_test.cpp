/**
 * beacon
 * Author: Lukas Krejci <krejci.l@centrum.cz>, (C) 2008
 * Copyright: See COPYING file that comes with this distribution
 */

#include <beacons/event_queue.hpp>
#define BOOST_TEST_MODULE event_queue
#define BOOST_TEST_MAIN
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

class test_inv : public beacons::invokable {
    public:

        test_inv() : beacons::invokable(beacons::invokable::token_ptr(new beacons::invocation_token)) {}

        test_inv(beacons::invokable::token_ptr token) : beacons::invokable(token) {
        }

        void invoke() {
            AO_fetch_and_add1(&test_inv::_invocations);
        }

        size_t invocations() {
            return (size_t)test_inv::_invocations;
        }

        static void reset_invocations() {
            AO_store(&test_inv::_invocations, 0);
        }

    private:
        static volatile AO_t _invocations;
};

volatile AO_t test_inv::_invocations = 0;

BOOST_AUTO_TEST_CASE(enqueue) {

    beacons::event_queue q;

    test_inv::reset_invocations();

    test_inv test;
    test_inv * inv1 = new test_inv;
    test_inv * inv2 = new test_inv;

    q.enqueue(inv1);
    q.enqueue(inv2);

    q.invoke_enqueued();

    BOOST_CHECK(test.invocations() == 2);
}

BOOST_AUTO_TEST_CASE(dequeue) {
    beacons::event_queue q;

    test_inv::reset_invocations();

    test_inv test;
    test_inv * inv1 = new test_inv;
    test_inv * inv2 = new test_inv;

    q.enqueue(inv1);
    q.enqueue(inv2);

    q.dequeue(inv1->token());

    q.invoke_enqueued();

    BOOST_CHECK(test.invocations() == 1);
}
